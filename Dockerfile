ARG IMAGE_REGISTRY="gitlab-registry.ifremer.fr/ifremer-commons/docker/images/"
ARG DOCKER_IMAGE="php:7.4-apache"
FROM $IMAGE_REGISTRY$DOCKER_IMAGE

ARG USERNAME="www-user"
ARG USERID="207506"
ARG GROUPNAME="www-user"
ARG GROUPID="11679"

RUN \
    echo "Creation group ${GROUPNAME}:${GROUPID}" && \
    groupadd -g ${GROUPID} ${GROUPNAME} && \
    echo "Creation user ${USERNAME}:${USERID}" && \
    useradd -g ${GROUPNAME} -u ${USERID} ${USERNAME}

RUN \
    apt-get update \
    && apt-get install -y \
    && apt-get autoremove -y \
    && docker-php-ext-install mysqli pdo pdo_mysql \
    && apt-get install curl -y \
    && apt-get install git -y\
    && apt-get install zip -y\
    && curl -sS https://get.symfony.com/cli/installer | bash \
    && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony \
    && rm /etc/apache2/sites-enabled/000-default.conf

WORKDIR /var/www/html
COPY --chown=www-data:www-data docker/vhosts/vhosts.conf /etc/apache2/sites-enabled/vhosts.conf
COPY --chown=www-data:www-data . /var/www/html
COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN \
    composer install && \
    usermod -u 1000 www-data && \
    mkdir -p var/cache/prod var/cache/dev var/cache/test var/log && \
    chown -R www-data:www-data var/ && \
    chmod -R a+rwx var/
