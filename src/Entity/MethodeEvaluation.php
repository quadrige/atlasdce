<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MethodeEvaluation
 *
 * @ORM\Table(name="METHODE_EVALUATION")
 * @ORM\Entity
 */
class MethodeEvaluation
{
    /**
     * @var int
     *
     * @ORM\Column(name="METHODE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $methodeId;

    /**
     * @var string
     *
     * @ORM\Column(name="METHODE_LIBELLE", type="text", length=65535, nullable=false)
     */
    private $methodeLibelle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $typeClassementId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $elementQualiteId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TYPE_ELEMENT_QUALITE_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $typeElementQualiteId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true)
     */
    private $bassinId;


}
