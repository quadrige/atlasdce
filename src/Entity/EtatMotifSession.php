<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtatMotifSession
 *
 * @ORM\Table(name="ETAT_MOTIF_SESSION", indexes={@ORM\Index(name="MOTIF_BASSIN_FK", columns={"BASSIN_ID"}), @ORM\Index(name="ETATMOTIF_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class EtatMotifSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="MOTIF_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $motifId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MOTIF_LIBELLE", type="string", length=255, nullable=false)
     */
    private $motifLibelle = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="MOTIF_CODE", type="string", length=10, nullable=true)
     */
    private $motifCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;


}
