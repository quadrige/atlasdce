<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MasseEauDept
 *
 * @ORM\Table(name="MASSE_EAU_DEPT", indexes={@ORM\Index(name="MASSE_EAU_DEPT_MASSE_FK", columns={"MASSE_ID"}), @ORM\Index(name="MASSE_EAU_DEPT_DEPT_FK", columns={"DEPT_ID"})})
 * @ORM\Entity
 */
class MasseEauDept
{
    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masseId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="DEPT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $deptId = '0';


}
