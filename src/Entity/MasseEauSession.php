<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MasseEauSession
 *
 * @ORM\Table(name="MASSE_EAU_SESSION", indexes={@ORM\Index(name="MASSE_EAU_SESSION__REGION_FK", columns={"REGION_ID"}), @ORM\Index(name="ME_SESSION_ATTEINTE_OBJ_FK", columns={"MASSE_B_ATTEINTE_OBJ"}), @ORM\Index(name="MASSE_EAU_SESSION_DESC_FK", columns={"MASSE_DESC_ID"}), @ORM\Index(name="MASSE_EAU_SESSION__BASSIN_FK", columns={"BASSIN_ID"}), @ORM\Index(name="MASSE_EAU_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class MasseEauSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masseId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MASSE_CODE", type="string", length=20, nullable=false)
     */
    private $masseCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MASSE_NOM", type="string", length=255, nullable=false)
     */
    private $masseNom = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_TYPE", type="string", length=45, nullable=true)
     */
    private $masseType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_TYPE_SUIVI", type="string", length=45, nullable=true)
     */
    private $masseTypeSuivi;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBSuivi;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_RISQUE", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBRisque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_URL_FICHE", type="string", length=255, nullable=true)
     */
    private $masseUrlFiche;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_DESC_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseDescId;

    /**
     * @var int
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $bassinId = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="REGION_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $regionId = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_RANG_GEO", type="integer", nullable=true)
     */
    private $masseRangGeo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI_OP", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBSuiviOp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_ATTEINTE_OBJECTIF", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBAtteinteObjectif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_OBJECTIF", type="string", length=255, nullable=true)
     */
    private $masseObjectif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_REPORT_OBJECTIF", type="string", length=255, nullable=true)
     */
    private $masseReportObjectif;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_FORT_MODIF", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBFortModif;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="MASSE_DATE_FORT_MODIF", type="datetime", nullable=true)
     */
    private $masseDateFortModif;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI_ENQUETE", type="integer", nullable=true)
     */
    private $masseBSuiviEnquete;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI_SURVEILLANCE", type="integer", nullable=true)
     */
    private $masseBSuiviSurveillance;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_DOC_DESC", type="string", length=255, nullable=true)
     */
    private $masseDocDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_COMMENTAIRE", type="text", length=65535, nullable=true)
     */
    private $masseCommentaire;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_ATTEINTE_OBJ", type="integer", nullable=true)
     */
    private $masseBAtteinteObj;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_MOTIF_DEROGATION", type="string", length=100, nullable=true)
     */
    private $masseMotifDerogation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_DOC_FICHE", type="string", length=255, nullable=true)
     */
    private $masseDocFiche;


}
