<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtatSession
 *
 * @ORM\Table(name="ETAT_SESSION", indexes={@ORM\Index(name="ETAT_TYPE_CLASSEMENT_FK", columns={"TYPE_CLASSEMENT_ID"}), @ORM\Index(name="ETAT_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class EtatSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="ETAT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $etatId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $typeClassementId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ETAT_LIBELLE", type="string", length=255, nullable=false)
     */
    private $etatLibelle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ETAT_COULEUR", type="string", length=20, nullable=false)
     */
    private $etatCouleur = '';

    /**
     * @var int
     *
     * @ORM\Column(name="ETAT_VALEUR", type="integer", nullable=false)
     */
    private $etatValeur = '0';


}
