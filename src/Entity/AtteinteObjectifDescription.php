<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AtteinteObjectifDescription
 *
 * @ORM\Table(name="ATTEINTE_OBJECTIF_DESCRIPTION")
 * @ORM\Entity(repositoryClass="App\Repository\AtteinteObjectifRepository")
 */
class AtteinteObjectifDescription
{
    /**
     * @var int
     *
     * @ORM\Column(name="ATTEINTE_OBJECTIF_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $atteinteObjectifId;

    /**
     * @var string
     *
     * @ORM\Column(name="ATTEINTE_OBJECTIF_LIBELLE", type="string", length=255, nullable=false)
     */
    private $atteinteObjectifLibelle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ATTEINTE_OBJECTIF_CODE", type="string", length=10, nullable=true)
     */
    private $atteinteObjectifCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ATTEINTE_OBJECTIF_COLOR", type="string", length=10, nullable=true)
     */
    private $atteinteObjectifColor;

    /**
     * @ORM\OneToMany(targetEntity=MasseEau::class, mappedBy="atteinteObjectifDescription")
     * @ORM\JoinColumn(name="MASSE_B_ATTEINTE_OBJ", referencedColumnName="ATTEINTE_OBJECTIF_ID")
     */
    private $massesEau;

    public function __construct()
    {
        $this->massesEau = new ArrayCollection();
    }

    /**
     * Retourne "ATTEINTE_OBJECTIF_ID"
     *
     * @return integer|null
     */
    public function getAtteinteObjectifId(): ?int
    {
        return $this->atteinteObjectifId;
    }

    /**
     * Retourne "ATTEINTE_OBJECTIF_LIBELLE"
     *
     * @return string|null
     */
    public function getAtteinteObjectifLibelle(): ?string
    {
        return $this->atteinteObjectifLibelle;
    }

    /**
     * Retourne "ATTEINTE_OBJECTIF_CODE"
     */
    public function getAtteinteObjectifCode(): ?string
    {
        return $this->atteinteObjectifCode;
    }

    /**
     * Retourne "ATTEINTE_OBJECTIF_COLOR"
     *
     * @return string|null
     */
    public function getAtteinteObjectifColor(): ?string
    {
        return $this->atteinteObjectifColor;
    }

    /**
     * @return Collection|MasseEau[]
     */
    public function getMassesEau(): Collection
    {
        return $this->massesEau;
    }

}
