<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeElementQualite
 *
 * @ORM\Table(name="TYPE_ELEMENT_QUALITE")
 * @ORM\Entity(repositoryClass="App\Repository\TypeElementQualiteRepository")
 */
class TypeElementQualite
{
    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_ELEMENT_QUALITE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $typeElementQualiteId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE_ELEMENT_QUALITE_NOM", type="string", length=255, nullable=false)
     */
    private $typeElementQualiteNom = '';

    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=false)
     */
    private $typeClassementId = '0';

    /**
     * @ORM\OneToMany(targetEntity=ElementQualite::class, mappedBy="typeElementQualite")
     * @ORM\JoinColumn(name="TYPE_ELEMENT_QUALITE_ID", referencedColumnName="TYPE_ELEMENT_QUALITE_ID")
     */
    private $elementQualites;

    /**
     * @ORM\ManyToMany(targetEntity=ElementQualiteSession::class, mappedBy="typeElementQualite")
     * @ORM\JoinTable(name="ELEMENT_QUALITE_SESSION",
     *  joinColumns={@ORM\JoinColumn(name="TYPE_ELEMENT_QUALITE_ID", referencedColumnName="TYPE_ELEMENT_QUALITE_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="TYPE_ELEMENT_QUALITE_ID", referencedColumnName="TYPE_ELEMENT_QUALITE_ID")}
     * )
     */
    private $elementQualiteSessions;

    public function __construct()
    {
        $this->elementQualites = new ArrayCollection();
        $this->elementQualiteSessions = new ArrayCollection();
    }

    /**
     * @return Collection|ElementQualite[]
     */
    public function getElementQualites(): Collection
    {
        return $this->elementQualites;
    }

    /**
     * Retourne "TYPE_ELEMENT_QUALITE_ID"
     *
     * @return integer|null
     */
    public function getTypeElementQualiteId(): ?int
    {
        return $this->typeElementQualiteId;
    }

    /**
     * Retourne "TYPE_ELEMENT_QUALITE_NOM"
     *
     * @return string|null
     */
    public function getTypeElementQualiteNom(): ?string
    {
        return $this->typeElementQualiteNom;
    }

    /**
     * Retourne "TYPE_CLASSEMENT_ID"
     *
     * @return integer|null
     */
    public function getTypeClassementId(): ?int
    {
        return $this->typeClassementId;
    }

    /**
     * @return Collection|ElementQualiteSession[]
     */
    public function getElementQualiteSessions(): Collection
    {
        return $this->elementQualiteSessions;
    }

}
