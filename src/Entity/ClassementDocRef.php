<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassementDocRef
 *
 * @ORM\Table(name="CLASSEMENT_DOC_REF", indexes={@ORM\Index(name="DOC_REF_ELEMENT_QUALITE_FK", columns={"ELEMENT_QUALITE_ID"}), @ORM\Index(name="DOC_REF_SESSION_QUALITE_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class ClassementDocRef
{
    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $elementQualiteId = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_REF", type="string", length=255, nullable=true)
     */
    private $docRef;


}
