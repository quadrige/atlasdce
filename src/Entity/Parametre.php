<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Parametre
 *
 * @ORM\Table(name="PARAMETRE", indexes={@ORM\Index(name="SUPPORT_ID", columns={"SUPPORT_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\ParametreRepository")
 */
class Parametre
{
    /**
     * @var int
     *
     * @ORM\Column(name="PARAMETRE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $parametreId;

    /**
     * @var int
     *
     * @ORM\Column(name="SUPPORT_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $supportId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PARAMETRE_CODE", type="string", length=20, nullable=false)
     */
    private $parametreCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PARAMETRE_NOM", type="string", length=255, nullable=false)
     */
    private $parametreNom = '';

    /**
     * @ORM\ManyToMany(targetEntity=Point::class, inversedBy="parametres")
     * @ORM\JoinTable(name="RESEAU_POINT_PARAM",
     *  joinColumns={@ORM\JoinColumn(name="PARAMETRE_ID", referencedColumnName="PARAMETRE_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="POINT_ID", referencedColumnName="POINT_ID")}
     * )
     */
    private $points;

    /**
     * @ORM\ManyToMany(targetEntity=Reseau::class, inversedBy="parametres")
     * @ORM\JoinTable(name="RESEAU_POINT_PARAM",
     *  joinColumns={@ORM\JoinColumn(name="PARAMETRE_ID", referencedColumnName="PARAMETRE_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="RESEAU_ID", referencedColumnName="RESEAU_ID")}
     * )
     */
    private $reseaux;

    public function __construct()
    {
        $this->points = new ArrayCollection();
        $this->reseaux = new ArrayCollection();
    }

    /**
     * Retourne "PARAMETRE_ID"
     *
     * @return integer|null
     */
    public function getParametreId(): ?int
    {
        return $this->parametreId;
    }

    /**
     * Retourne "SUPPORT_ID"
     *
     * @return integer|null
     */
    public function getSupportId(): ?int
    {
        return $this->supportId;
    }

    /**
     * Retourne "PARAMETRE_CODE"
     *
     * @return string|null
     */
    public function getParametreCode(): ?string
    {
        return $this->parametreCode;
    }

    /**
     * Retourne "PARAMETRE_NOM"
     *
     * @return string|null
     */
    public function getParametreNom(): ?string
    {
        return $this->parametreNom;
    }

    /**
     * @return Collection|Point[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    /**
     * @return Collection|Reseau[]
     */
    public function getReseaux(): Collection
    {
        return $this->reseaux;
    }


}
