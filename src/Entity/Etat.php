<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Etat
 *
 * @ORM\Table(name="ETAT", indexes={@ORM\Index(name="ETAT_TYPE_CLASSEMENT_FK", columns={"TYPE_CLASSEMENT_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @var int
     *
     * @ORM\Column(name="ETAT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $etatId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $typeClassementId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ETAT_LIBELLE", type="string", length=255, nullable=false)
     */
    private $etatLibelle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ETAT_COULEUR", type="string", length=20, nullable=false)
     */
    private $etatCouleur = '';

    /**
     * @var int
     *
     * @ORM\Column(name="ETAT_VALEUR", type="integer", nullable=false)
     */
    private $etatValeur = '0';

    /**
     * @ORM\ManyToMany(targetEntity=MasseEau::class, mappedBy="etats")
     */
    private $massesEau;

    public function __construct()
    {
        $this->elementQualites = new ArrayCollection();
        $this->massesEau = new ArrayCollection();
    }

    /**
     * Retourne "ETAT_ID"
     *
     * @return integer|null
     */
    public function getEtatId(): ?int
    {
        return $this->etatId;
    }

    /**
     * Retourne "TYPE_CLASSEMENT_ID"
     *
     * @return integer|null
     */
    public function getTypeClassementId(): ?int
    {
        return $this->typeClassementId;
    }

    /**
     * Retourne "ETAT_LIBELLE"
     *
     * @return string|null
     */
    public function getEtatLibelle(): ?string
    {
        return $this->etatLibelle;
    }

    /**
     * Retourne "ETAT_COULEUR"
     *
     * @return string|null
     */
    public function getEtatCouleur(): ?string
    {
        return $this->etatCouleur;
    }

    /**
     * Retourne "ETAT_VALEUR"
     *
     * @return integer|null
     */
    public function getEtatValeur(): ?int
    {
        return $this->etatValeur;
    }

    /**
     * @return Collection|MasseEau[]
     */
    public function getMassesEau(): Collection
    {
        return $this->massesEau;
    }

}
