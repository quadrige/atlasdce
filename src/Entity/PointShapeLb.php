<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointShapeLb
 *
 * @ORM\Table(name="POINT_SHAPE_LB")
 * @ORM\Entity
 */
class PointShapeLb
{
    /**
     * @var string
     *
     * @ORM\Column(name="POINT_CODE", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pointCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="POINT_NOM", type="string", length=200, nullable=false)
     */
    private $pointNom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ME_CODE", type="string", length=50, nullable=false)
     */
    private $meCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="POINT_LONGITUDE", type="string", length=100, nullable=false)
     */
    private $pointLongitude = '';

    /**
     * @var string
     *
     * @ORM\Column(name="POINT_LATITUDE", type="string", length=100, nullable=false)
     */
    private $pointLatitude = '0';


}
