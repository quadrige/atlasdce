<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SessionQualite
 *
 * @ORM\Table(name="SESSION_QUALITE", indexes={@ORM\Index(name="SESSION_BASSIN_FK", columns={"BASSIN_ID"}), @ORM\Index(name="SESSION_USERCREA_FK", columns={"SESSION_USER_CREA"})})
 * @ORM\Entity
 */
class SessionQualite
{
    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="SESSION_LIBELLE", type="string", length=255, nullable=false)
     */
    private $sessionLibelle = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="SESSION_DATE_CREA", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $sessionDateCrea = '0000-00-00 00:00:00';

    /**
     * @var int|null
     *
     * @ORM\Column(name="SESSION_USER_CREA", type="integer", nullable=true)
     */
    private $sessionUserCrea;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SESSION_ARCHIVE", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $sessionArchive = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="SESSION_DATE_ARCHIVE", type="datetime", nullable=true)
     */
    private $sessionDateArchive;


}
