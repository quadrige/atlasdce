<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Departement
 *
 * @ORM\Table(name="DEPARTEMENT")
 * @ORM\Entity(repositoryClass="App\Repository\DepartementRepository")
 */
class Departement
{
    /**
     * @var int
     *
     * @ORM\Column(name="DEPT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $deptId;

    /**
     * @var string
     *
     * @ORM\Column(name="DEPT_NOM", type="string", length=255, nullable=false)
     */
    private $deptNom = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DEPT_CODE", type="string", length=20, nullable=false)
     */
    private $deptCode = '';

    /**
     * @var int
     *
     * @ORM\Column(name="DEPT_VISIBLE", type="integer", nullable=false)
     */
    private $deptVisible = '0';

    /**
     * @ORM\ManyToMany(targetEntity=MasseEau::class, mappedBy="departement")
     */
    private $massesEau;

    public function __construct()
    {
        $this->massesEau = new ArrayCollection();
    }

    /**
     * @return Collection|MasseEau[]
     */
    public function getMassesEau(): Collection
    {
        return $this->massesEau;
    }

    public function getDeptId(): ?int
    {
        return $this->deptId;
    }

    public function getDeptNom(): ?string
    {
        return $this->deptNom;
    }

    public function getDeptCode(): ?string
    {
        return $this->deptCode;
    }

    public function getDeptVisible(): ?bool
    {
        return $this->deptVisible ? true : false;
    }

}
