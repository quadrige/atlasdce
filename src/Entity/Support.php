<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Support
 *
 * @ORM\Table(name="SUPPORT", indexes={@ORM\Index(name="RESEAU_ID", columns={"RESEAU_ID"})})
 * @ORM\Entity
 */
class Support
{
    /**
     * @var int
     *
     * @ORM\Column(name="SUPPORT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $supportId;

    /**
     * @var int
     *
     * @ORM\Column(name="RESEAU_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $reseauId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SUPPORT_CODE", type="string", length=20, nullable=false)
     */
    private $supportCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SUPPORT_NOM", type="string", length=255, nullable=false)
     */
    private $supportNom = '';


}
