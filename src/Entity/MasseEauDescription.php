<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MasseEauDescription
 *
 * @ORM\Table(name="MASSE_EAU_DESCRIPTION")
 * @ORM\Entity(repositoryClass="App\Repository\MasseEauDescriptionRepository")
 */
class MasseEauDescription
{
    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_DESC_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $masseDescId;

    /**
     * @var string
     *
     * @ORM\Column(name="MASSE_DESC_LIBELLE", type="string", length=255, nullable=false)
     */
    private $masseDescLibelle = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_DESC_CODE", type="string", length=10, nullable=true)
     */
    private $masseDescCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_DESC_COLOR", type="string", length=10, nullable=true)
     */
    private $masseDescColor;

    /**
     * @var string
     *
     * @ORM\Column(name="MASSE_DESC_TYPE", type="string", length=1, nullable=false, options={"default"="C","fixed"=true})
     */
    private $masseDescType = 'C';

    /**
     * @ORM\OneToMany(targetEntity=MasseEau::class, mappedBy="masseEauDescription")
     * @ORM\JoinColumn(name="MASSE_DESC_ID", referencedColumnName="MASSE_DESC_ID")
     */
    private $massesEau;

    public function __construct()
    {
        $this->massesEau = new ArrayCollection();
    }

    /**
     * @return Collection|MasseEau[]
     */
    public function getMassesEau(): Collection
    {
        return $this->massesEau;
    }

    public function getMasseDescId(): ?int
    {
        return $this->masseDescId;
    }

    public function getMasseDescLibelle(): ?string
    {
        return $this->masseDescLibelle;
    }

    public function getMasseDescCode(): ?string
    {
        return $this->masseDescCode;
    }


    public function getMasseDescColor(): ?string
    {
        return $this->masseDescColor;
    }

    public function getMasseDescType(): ?string
    {
        return $this->masseDescType;
    }

}
