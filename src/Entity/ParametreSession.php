<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParametreSession
 *
 * @ORM\Table(name="PARAMETRE_SESSION", indexes={@ORM\Index(name="SUPPORT_ID", columns={"SUPPORT_ID"}), @ORM\Index(name="PARAMETRE_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class ParametreSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="PARAMETRE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $parametreId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SUPPORT_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $supportId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PARAMETRE_CODE", type="string", length=20, nullable=false)
     */
    private $parametreCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PARAMETRE_NOM", type="string", length=255, nullable=false)
     */
    private $parametreNom = '';


}
