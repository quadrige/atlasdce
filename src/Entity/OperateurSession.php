<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OperateurSession
 *
 * @ORM\Table(name="OPERATEUR_SESSION", indexes={@ORM\Index(name="OPERATEUR_BASSIN_FK", columns={"BASSIN_ID"}), @ORM\Index(name="OPERATEUR_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class OperateurSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="OPERATEUR_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $operateurId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="OPERATEUR_NOM", type="string", length=100, nullable=false)
     */
    private $operateurNom = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="OPERATEUR_CODE", type="string", length=20, nullable=true)
     */
    private $operateurCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;


}
