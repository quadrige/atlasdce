<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UlidGenerator;

/**
 * ReseauPointParam
 * 
 * @ORM\Table(name="RESEAU_POINT_PARAM", indexes={@ORM\Index(name="RESEAU_ID", columns={"RESEAU_ID"}), @ORM\Index(name="POINT_ID", columns={"PARAMETRE_ID"}), @ORM\Index(name="OPERATEUR_TERRAIN", columns={"OPERATEUR_TERRAIN"}), @ORM\Index(name="OPERATEUR_LABO", columns={"OPERATEUR_LABO"})})
 * @ORM\Entity(repositoryClass=ReseauPointParamRepository::class)
 */
class ReseauPointParam
{
    /**
     * @ORM\Id
     * @ORM\Column(unique=false, name="POINT_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $pointId;

    /**
     * @ORM\Column(name="PARAMETRE_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $parametreId;

    /**
     * @ORM\Column(name="RESEAU_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $reseauId;

    /**
     * @ORM\Column(name="OPERATEUR_TERRAIN", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $operateurTerrainId;

    /**
     * @ORM\Column(name="OPERATEUR_LABO", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $operateurLaboId;

    /**
     * @ORM\Column(name="FREQUENCE", type="string", length=100, nullable=true)
     */
    private $frequence;

    /**
     * @ORM\Column(name="ANNEE_PRELEVEMENT", type="string", length=255, nullable=true)
     */
    private $anneePrelevement;

    /**
     * @ORM\Column(name="FREQUENCE_PLAN", type="string", length=50, nullable=true)
     */
    private $frequencePlan;

    /**
     * @ORM\Column(name="PERIODE", type="string", length=100, nullable=true)
     */
    private $periode;

    /**
     * @ORM\Column(name="RESEAU_DATE_MAJ", type="datetime", nullable=true)
     */
    private $reseauDateMaj;

    /**
     * Retourne "POINT_ID"
     *
     * @return integer|null
     */
    public function getPointId(): ?int
    {
        return $this->pointId;
    }

    /**
     * Retourne "PARAMETRE_ID"
     *
     * @return integer|null
     */
    public function getParametreId(): ?int
    {
        return $this->parametreId;
    }

    /**
     * Retourne "RESEAU_ID"
     *
     * @return integer|null
     */
    public function getReseauId(): ?int
    {
        return $this->reseauId;
    }

    /**
     * Retourne "OPERATEUR_TERRAIN"
     *
     * @return integer|null
     */
    public function getOperateurTerrainId(): ?int
    {
        return $this->operateurTerrainId;
    }

    /**
     * Retourne "OPERATEUR_LABO"
     *
     * @return integer|null
     */
    public function getOperateurLaboId(): ?int
    {
        return $this->operateurLaboId;
    }

    /**
     * Retourne "FREQUENCE"
     *
     * @return string|null
     */
    public function getFrequence(): ?string
    {
        return $this->frequence;
    }

    /**
     * Retourne "ANNEE_PRELEVEMENT"
     *
     * @return string|null
     */
    public function getAnneePrelevement(): ?string
    {
        return $this->anneePrelevement;
    }

    /**
     * Retourne "FREQUENCE_PLAN"
     *
     * @return string|null
     */
    public function getFrequencePlan(): ?string
    {
        return $this->frequencePlan;
    }

    /**
     * Retourne "PERIODE"
     *
     * @return string|null
     */
    public function getPeriode(): ?string
    {
        return $this->periode;
    }

    /**
     * Retourne "RESEAU_DATE_MAJ"
     *
     * @return \DateTimeInterface|null
     */
    public function getReseauDateMaj(): ?\DateTimeInterface
    {
        return $this->reseauDateMaj;
    }

}
