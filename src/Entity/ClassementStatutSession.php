<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassementStatutSession
 *
 * @ORM\Table(name="CLASSEMENT_STATUT_SESSION", indexes={@ORM\Index(name="STATUT_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class ClassementStatutSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="STATUT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $statutId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATUT_CODE", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private $statutCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="STATUT_LIBELLE", type="string", length=255, nullable=false)
     */
    private $statutLibelle = '0';


}
