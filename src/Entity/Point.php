<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Point
 *
 * @ORM\Table(name="POINT", uniqueConstraints={@ORM\UniqueConstraint(name="POINT_UNIQ_CODE", columns={"POINT_CODE"})}, indexes={@ORM\Index(name="MASSE_ID", columns={"MASSE_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\PointRepository")
 */
class Point
{
    /**
     * @var int
     *
     * @ORM\Column(name="POINT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pointId;

    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $masseId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="POINT_CODE", type="string", length=50, nullable=false)
     */
    private $pointCode = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_NOM", type="string", length=255, nullable=true)
     */
    private $pointNom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_LONGITUDE", type="string", length=20, nullable=true)
     */
    private $pointLongitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_LATITUDE", type="string", length=20, nullable=true)
     */
    private $pointLatitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_URL_FICHE", type="string", length=255, nullable=true)
     */
    private $pointUrlFiche;

    /**
     * @var int
     *
     * @ORM\Column(name="POINT_MAJ_COORD", type="integer", nullable=false)
     */
    private $pointMajCoord = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="OLD_POINT_CODE", type="string", length=50, nullable=true)
     */
    private $oldPointCode;

    /**
     * @ORM\ManyToOne(targetEntity=MasseEau::class, inversedBy="points")
     * @ORM\JoinColumn(name="MASSE_ID", referencedColumnName="MASSE_ID")
     */
    private $masseEau;

    /**
     * @ORM\ManyToMany(targetEntity=Reseau::class, mappedBy="points")
     */
    private $reseaux;

    /**
     * @ORM\ManyToMany(targetEntity=Parametre::class, mappedBy="points")
     */
    private $parametres;

    /**
     * @ORM\ManyToMany(targetEntity=ReseauSession::class, mappedBy="points")
     */
    private $reseauxSessions;

    public function __construct()
    {
        $this->reseaux = new ArrayCollection();
        $this->parametres = new ArrayCollection();
        $this->reseauxSessions = new ArrayCollection();
    }

    /**
     * Retourne "POINT_ID"
     *
     * @return integer|null
     */
    public function getPointId(): ?int
    {
        return $this->pointId;
    }

    /**
     * Retourne "MASSE_ID"
     *
     * @return integer|null
     */
    public function getMasseId(): ?int
    {
        return $this->masseId;
    }

    /**
     * Retourne "POINT_CODE"
     *
     * @return string|null
     */
    public function getPointCode(): ?string
    {
        return $this->pointCode;
    }

    /**
     * Retourne "POINT_NOM"
     *
     * @return string|null
     */
    public function getPointNom(): ?string
    {
        return $this->pointNom;
    }

    /**
     * Retourne "POINT_LONGITUDE"
     *
     * @return string|null
     */
    public function getPointLongitude(): ?string
    {
        return $this->pointLongitude;
    }

    /**
     * Retourne "POINT_LATITUDE"
     *
     * @return string|null
     */
    public function getPointLatitude(): ?string
    {
        return $this->pointLatitude;
    }

    /**
     * Retourne "POINT_URL_FICHE"
     *
     * @return string|null
     */
    public function getPointUrlFiche(): ?string
    {
        return $this->pointUrlFiche;
    }

    /**
     * Retourne "POINT_MAJ_COORD"
     *
     * @return boolean|null
     */
    public function getPointMajCoord(): ?bool
    {
        return $this->pointMajCoord ? true : false;
    }

    /**
     * Retourne "OLD_POINT_CODE"
     *
     * @return string|null
     */
    public function getOldPointCode(): ?string
    {
        return $this->oldPointCode;
    }

    public function getMasseEau(): ?MasseEau
    {
        return $this->masseEau;
    }

    /**
     * @return Collection|Reseau[]
     */
    public function getReseaux(): Collection
    {
        return $this->reseaux;
    }

    /**
     * @return Collection|Parametre[]
     */
    public function getParametres(): Collection
    {
        return $this->parametres;
    }

    /**
     * @return Collection|ReseauSession[]
     */
    public function getReseauxSessions(): Collection
    {
        return $this->reseauxSessions;
    }

}
