<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtatMotif
 *
 * @ORM\Table(name="ETAT_MOTIF", indexes={@ORM\Index(name="MOTIF_BASSIN_FK", columns={"BASSIN_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\EtatMotifRepository")
 */
class EtatMotif
{
    /**
     * @var int
     *
     * @ORM\Column(name="MOTIF_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $motifId;

    /**
     * @var string
     *
     * @ORM\Column(name="MOTIF_LIBELLE", type="string", length=255, nullable=false)
     */
    private $motifLibelle = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="MOTIF_CODE", type="string", length=10, nullable=true)
     */
    private $motifCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="OLD_MOTIF_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $oldMotifId;

    /**
     * @ORM\ManyToOne(targetEntity=BassinHydrographique::class, inversedBy="etatMotif")
     * @ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")
     */
    private $bassinHydrographique;

    public function getMotifId(): ?int
    {
        return $this->motifId;
    }

    public function getMotifLibelle(): ?string
    {
        return $this->motifLibelle;
    }

    public function getMotifCode(): ?string
    {
        return $this->motifCode;
    }

    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

    public function getOldMotifId(): ?int
    {
        return $this->oldMotifId;
    }

    public function getBassinHydrographique(): ?BassinHydrographique
    {
        return $this->bassinHydrographique;
    }

    public function setBassinHydrographique(?BassinHydrographique $bassinHydrographique): self
    {
        $this->bassinHydrographique = $bassinHydrographique;

        return $this;
    }


}
