<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MasseEauSessionSuivi
 *
 * @ORM\Table(name="MASSE_EAU_SESSION_SUIVI", indexes={@ORM\Index(name="MASSE_EAU_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class MasseEauSessionSuivi
{
    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masseId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MASSE_TYPE_SUIVI", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $masseTypeSuivi = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI_NBPOINT", type="integer", nullable=true)
     */
    private $masseBSuiviNbpoint = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_LIST_RESEAU", type="text", length=65535, nullable=true)
     */
    private $masseListReseau;


}
