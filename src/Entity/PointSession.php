<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointSession
 *
 * @ORM\Table(name="POINT_SESSION", indexes={@ORM\Index(name="MASSE_ID", columns={"MASSE_ID"}), @ORM\Index(name="POINT_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class PointSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="POINT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pointId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $masseId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="POINT_CODE", type="string", length=50, nullable=false)
     */
    private $pointCode = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_NOM", type="string", length=255, nullable=true)
     */
    private $pointNom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_LONGITUDE", type="string", length=20, nullable=true)
     */
    private $pointLongitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_LATITUDE", type="string", length=20, nullable=true)
     */
    private $pointLatitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POINT_URL_FICHE", type="string", length=255, nullable=true)
     */
    private $pointUrlFiche;


}
