<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ReseauGroupe
 *
 * @ORM\Table(name="RESEAU_GROUPE")
 * @ORM\Entity(repositoryClass="App\Repository\ReseauGroupeRepository")
 */
class ReseauGroupe
{
    /**
     * @var int
     *
     * @ORM\Column(name="GROUPE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $groupeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="GROUPE_NOM", type="string", length=255, nullable=false)
     */
    private $groupeNom = '';

    /**
     * @var int
     *
     * @ORM\Column(name="GROUPE_B_PROG_SURV", type="integer", nullable=false)
     */
    private $groupeBProgSurv = '0';

    /**
     * @ORM\OneToMany(targetEntity=Reseau::class, mappedBy="reseauGroupe")
     */
    private $reseaux;

    public function __construct()
    {
        $this->reseaux = new ArrayCollection();
    }

    public function getGroupeId(): ?int
    {
        return $this->groupeId;
    }

    public function getGroupeNom(): ?string
    {
        return $this->groupeNom;
    }

    public function getGroupeBProgSurv(): ?int
    {
        return $this->groupeBProgSurv;
    }

    /**
     * @return Collection|Reseau[]
     */
    public function getReseaux(): Collection
    {
        return $this->reseaux;
    }

}
