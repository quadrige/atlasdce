<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SupportSession
 *
 * @ORM\Table(name="SUPPORT_SESSION", indexes={@ORM\Index(name="RESEAU_ID", columns={"RESEAU_ID"}), @ORM\Index(name="SUPPORT_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity
 */
class SupportSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="SUPPORT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $supportId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="RESEAU_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $reseauId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SUPPORT_CODE", type="string", length=20, nullable=false)
     */
    private $supportCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SUPPORT_NOM", type="string", length=255, nullable=false)
     */
    private $supportNom = '';


}
