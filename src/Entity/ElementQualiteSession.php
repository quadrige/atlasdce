<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ElementQualiteSession
 *
 * @ORM\Table(name="ELEMENT_QUALITE_SESSION", indexes={@ORM\Index(name="ELEMENT_QUALITE_TYPE_CLASSEMENT_FK", columns={"TYPE_CLASSEMENT_ID"}), @ORM\Index(name="ELEMENT_BASSIN_FK", columns={"BASSIN_ID"}), @ORM\Index(name="ELEMENT_QUALITE_TYPE_ELT_FK", columns={"TYPE_ELEMENT_QUALITE_ID"}), @ORM\Index(name="ETAT_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\ElementQualiteSessionRepository")
 */
class ElementQualiteSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $elementQualiteId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $typeClassementId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_ELEMENT_QUALITE_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $typeElementQualiteId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ELEMENT_QUALITE_NOM", type="string", length=255, nullable=false)
     */
    private $elementQualiteNom = '';

    /**
     * @var int
     *
     * @ORM\Column(name="ELEMENT_QUALITE_NIVEAU", type="integer", nullable=false, options={"default"="1","unsigned"=true})
     */
    private $elementQualiteNiveau = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="ELEMENT_QUALITE_PARENT", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $elementQualiteParent = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ARBRE", type="string", length=20, nullable=true)
     */
    private $elementQualiteArbre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_RANG", type="integer", nullable=true)
     */
    private $elementQualiteRang;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_URL_FICHE", type="string", length=255, nullable=true)
     */
    private $elementQualiteUrlFiche;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_DOC_PROTOCOLE", type="string", length=255, nullable=true)
     */
    private $elementQualiteDocProtocole;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_DOC_METHODO", type="string", length=255, nullable=true)
     */
    private $elementQualiteDocMethodo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_RESTRICT_TYPEME", type="string", length=10, nullable=true)
     */
    private $elementQualiteRestrictTypeme;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;

    /**
     * @ORM\ManyToMany(targetEntity=BassinHydrographique::class, mappedBy="elementQualiteSession")
     */
    private $bassinsHydrographiques;

    /**
     * @ORM\ManyToMany(targetEntity=TypeClassement::class, inversedBy="elementQualiteSessions")
     * @ORM\JoinTable(name="ELEMENT_QUALITE_SESSION",
     *  joinColumns={@ORM\JoinColumn(name="TYPE_CLASSEMENT_ID", referencedColumnName="TYPE_CLASSEMENT_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="TYPE_CLASSEMENT_ID", referencedColumnName="TYPE_CLASSEMENT_ID")}
     * )
     */
    private $typeClassement;

    /**
     * @ORM\ManyToMany(targetEntity=TypeElementQualite::class, inversedBy="elementQualiteSessions")
     * @ORM\JoinTable(name="ELEMENT_QUALITE_SESSION",
     *  joinColumns={@ORM\JoinColumn(name="TYPE_ELEMENT_QUALITE_ID", referencedColumnName="TYPE_ELEMENT_QUALITE_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="TYPE_ELEMENT_QUALITE_ID", referencedColumnName="TYPE_ELEMENT_QUALITE_ID")}
     * )
     */
    private $typeElementQualite;

    public function __construct()
    {
        $this->bassinsHydrographiques = new ArrayCollection();
        $this->typeClassement = new ArrayCollection();
        $this->typeElementQualite = new ArrayCollection();
    }

    public function getElementQualiteId(): ?int
    {
        return $this->elementQualiteId;
    }

    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    public function getTypeClassementId(): ?int
    {
        return $this->typeClassementId;
    }

    public function getTypeElementQualiteId(): ?int
    {
        return $this->typeElementQualiteId;
    }

    public function getElementQualiteNom(): ?string
    {
        return $this->elementQualiteNom;
    }

    public function getElementQualiteNiveau(): ?int
    {
        return $this->elementQualiteNiveau;
    }

    public function getElementQualiteParent(): ?int
    {
        return $this->elementQualiteParent;
    }

    public function getElementQualiteArbre(): ?string
    {
        return $this->elementQualiteArbre;
    }

    public function getElementQualiteRang(): ?int
    {
        return $this->elementQualiteRang;
    }

    public function getElementQualiteUrlFiche(): ?string
    {
        return $this->elementQualiteUrlFiche;
    }

    public function getElementQualiteDocProtocole(): ?string
    {
        return $this->elementQualiteDocProtocole;
    }

    public function getElementQualiteDocMethodo(): ?string
    {
        return $this->elementQualiteDocMethodo;
    }

    public function getElementQualiteRestrictTypeme(): ?string
    {
        return $this->elementQualiteRestrictTypeme;
    }

    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

    /**
     * @return Collection|BassinHydrographique[]
     */
    public function getBassinsHydrographiques(): Collection
    {
        return $this->bassinsHydrographiques;
    }

    /**
     * @return Collection|TypeClassement[]
     */
    public function getTypeClassement(): Collection
    {
        return $this->typeClassement;
    }

    /**
     * @return Collection|TypeElementQualite[]
     */
    public function getTypeElementQualite(): Collection
    {
        return $this->typeElementQualite;
    }

}
