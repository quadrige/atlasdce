<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sequence
 *
 * @ORM\Table(name="SEQUENCE")
 * @ORM\Entity
 */
class Sequence
{
    /**
     * @var int
     *
     * @ORM\Column(name="SEQUENCE_ID", type="integer", nullable=false, options={"default"="1"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sequenceId = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_MASSE_EAU", type="integer", nullable=false, options={"default"="15"})
     */
    private $seqMasseEau = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_OPERATEUR", type="integer", nullable=false, options={"default"="15"})
     */
    private $seqOperateur = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_PARAMETRE", type="integer", nullable=false, options={"default"="15"})
     */
    private $seqParametre = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_POINT", type="integer", nullable=false, options={"default"="15"})
     */
    private $seqPoint = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_RESEAU", type="integer", nullable=false, options={"default"="15"})
     */
    private $seqReseau = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_SUPPORT", type="integer", nullable=false, options={"default"="15"})
     */
    private $seqSupport = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_ANNU_USER", type="integer", nullable=false, options={"default"="15"})
     */
    private $seqAnnuUser = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_ETAT_MOTIF", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqEtatMotif = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_DEPARTEMENT", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqDepartement = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_MASSE_EAU_DESCRIPTION", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqMasseEauDescription = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_SESSION_QUALITE", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqSessionQualite = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_TYPE_ELEMENT_QUALITE", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqTypeElementQualite = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_BASSIN_HYDROGRAPHIQUE", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqBassinHydrographique = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_TYPE_CLASSEMENT", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqTypeClassement = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_CLASSEMENT_STATUT", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqClassementStatut = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_ETAT", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqEtat = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_ELEMENT_QUALITE", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqElementQualite = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_REGION_MARINE", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqRegionMarine = 1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SEQ_CLASSEMENT_MASSE_EAU", type="integer", nullable=true)
     */
    private $seqClassementMasseEau;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_METHODE_EVALUATION", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqMethodeEvaluation = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_RESEAU_GROUPE", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqReseauGroupe = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="SEQ_ATTEINTE_OBJECTIF_DESCRIPTION", type="integer", nullable=false, options={"default"="1"})
     */
    private $seqAtteinteObjectifDescription = 1;


}
