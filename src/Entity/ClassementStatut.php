<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassementStatut
 *
 * @ORM\Table(name="CLASSEMENT_STATUT")
 * @ORM\Entity(repositoryClass="App\Repository\ClassementStatutRepository")
 */
class ClassementStatut
{
    /**
     * @var int
     *
     * @ORM\Column(name="STATUT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $statutId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATUT_CODE", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private $statutCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="STATUT_LIBELLE", type="string", length=255, nullable=false)
     */
    private $statutLibelle = '0';

    public function getStatutId(): ?int
    {
        return $this->statutId;
    }

    public function getStatutCode(): ?string
    {
        return $this->statutCode;
    }

    public function getStatutLibelle(): ?string
    {
        return $this->statutLibelle;
    }

}
