<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeClassement
 *
 * @ORM\Table(name="TYPE_CLASSEMENT")
 * @ORM\Entity(repositoryClass="App\Repository\ElementQualiteRepository")
 */
class TypeClassement
{
    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $typeClassementId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_LIBELLE", type="string", length=255, nullable=false)
     */
    private $typeClassementLibelle = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_AFF", type="string", length=50, nullable=true)
     */
    private $typeClassementAff;

    /**
     * @ORM\OneToMany(targetEntity=ElementQualite::class, mappedBy="typeClassement")
     *  @ORM\JoinColumn(name="TYPE_CLASSEMENT_ID", referencedColumnName="TYPE_CLASSEMENT_ID")
     */
    private $elementQualites;

    /**
     * @ORM\ManyToMany(targetEntity=ElementQualiteSession::class, mappedBy="typeClassement")
     * @ORM\JoinTable(name="ELEMENT_QUALITE_SESSION",
     *  joinColumns={@ORM\JoinColumn(name="TYPE_CLASSEMENT_ID", referencedColumnName="TYPE_CLASSEMENT_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="TYPE_CLASSEMENT_ID", referencedColumnName="TYPE_CLASSEMENT_ID")}
     * )
     */
    private $elementQualiteSessions;

    public function __construct()
    {
        $this->elementQualites = new ArrayCollection();
        $this->elementQualiteSessions = new ArrayCollection();
    }

    public function getTypeClassementId(): ?int
    {
        return $this->typeClassementId;
    }

    public function getTypeClassementLibelle(): ?string
    {
        return $this->typeClassementLibelle;
    }

    public function getTypeClassementAff(): ?string
    {
        return $this->typeClassementAff;
    }

    /**
     * @return Collection|ElementQualite[]
     */
    public function getElementQualites(): Collection
    {
        return $this->elementQualites;
    }

    /**
     * @return Collection|ElementQualiteSession[]
     */
    public function getElementQualiteSessions(): Collection
    {
        return $this->elementQualiteSessions;
    }

}
