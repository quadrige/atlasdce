<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassementMasseEauSession
 *
 * @ORM\Table(name="CLASSEMENT_MASSE_EAU_SESSION", indexes={@ORM\Index(name="CLASSEMENT_MASSE_EAU_SESS_MASSE_FK", columns={"MASSE_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_SESS_ETAT_FK", columns={"ETAT_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_SESS_MOTIF_FK", columns={"MOTIF_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_SESS_TYPE_ELEMENT_FK", columns={"TYPE_ELEMENT_QUALITE_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_SESS_SESSION_FK", columns={"SESSION_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_SESS_STATUT_FK", columns={"STATUT_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_SESS_ELEMENT_FK", columns={"ELEMENT_QUALITE_ID"}), @ORM\Index(name="CLASSEMENT_SESS_TYPECLASSEMENT_FK", columns={"TYPE_CLASSEMENT_ID"})})
 * @ORM\Entity
 */
class ClassementMasseEauSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="CLASSEMENT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $classementId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $elementQualiteId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TYPE_ELEMENT_QUALITE_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $typeElementQualiteId;

    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $masseId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="MOTIF_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $motifId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="STATUT_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $statutId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ETAT_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $etatId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_DATE", type="datetime", nullable=true, options={"default"="0000-00-00 00:00:00"})
     */
    private $classementDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_DATE_CREA", type="datetime", nullable=true)
     */
    private $classementDateCrea;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_DATE_MAJ", type="datetime", nullable=true)
     */
    private $classementDateMaj;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CLASSEMENT_BILAN", type="text", length=65535, nullable=true)
     */
    private $classementBilan;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CLASSEMENT_COMPLEMENT_BILAN", type="string", length=255, nullable=true)
     */
    private $classementComplementBilan;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CLASSEMENT_DOC_REF", type="string", length=255, nullable=true)
     */
    private $classementDocRef;

    /**
     * @var int|null
     *
     * @ORM\Column(name="CLASSEMENT_INDICE_CONFIANCE", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $classementIndiceConfiance;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $typeClassementId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_ME_DATE_MAJ", type="datetime", nullable=true)
     */
    private $classementMeDateMaj;


}
