<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RegionMarine
 *
 * @ORM\Table(name="REGION_MARINE")
 * @ORM\Entity(repositoryClass="App\Repository\RegionMarineRepository")
 */
class RegionMarine
{
    /**
     * @var int
     *
     * @ORM\Column(name="REGION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $regionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="REGION_LIBELLE", type="string", length=255, nullable=false)
     */
    private $regionLibelle = '';

    /**
     * @ORM\OneToMany(targetEntity=MasseEau::class, mappedBy="regionMarine")
     * @ORM\JoinColumn(name="REGION_ID", referencedColumnName="REGION_ID")
     */
    private $massesEau;

    public function __construct()
    {
        $this->massesEau = new ArrayCollection();
    }

    public function getRegionId(): ?int
    {
        return $this->regionId;
    }

    public function getRegionLibelle(): ?string
    {
        return $this->regionLibelle;
    }

    /**
     * @return Collection|MasseEau[]
     */
    public function getMassesEau(): Collection
    {
        return $this->massesEau;
    }


}
