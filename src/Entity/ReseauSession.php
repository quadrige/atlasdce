<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ReseauSession
 *
 * @ORM\Table(name="RESEAU_SESSION", indexes={@ORM\Index(name="RESEAU_BASSIN_FK", columns={"BASSIN_ID"}), @ORM\Index(name="RESEAU_SESSION_SESS_FK", columns={"SESSION_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\ReseauSessionRepository")
 */
class ReseauSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="RESEAU_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $reseauId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RESEAU_CODE", type="string", length=20, nullable=false)
     */
    private $reseauCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RESEAU_NOM", type="string", length=100, nullable=false)
     */
    private $reseauNom = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="RESEAU_SYMBOLE", type="string", length=255, nullable=true)
     */
    private $reseauSymbole;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RESEAU_COULEUR", type="string", length=255, nullable=true)
     */
    private $reseauCouleur;

    /**
     * @var int|null
     *
     * @ORM\Column(name="RESEAU_RANG", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $reseauRang;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="GROUPE_ID", type="integer", nullable=false, options={"default"="-1"})
     */
    private $groupeId = -1;

    /**
     * @var int
     *
     * @ORM\Column(name="RESEAU_SYMBOLE_TAILLE", type="integer", nullable=false, options={"default"="1"})
     */
    private $reseauSymboleTaille = 1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RESEAU_IMG_SYMB", type="string", length=255, nullable=true)
     */
    private $reseauImgSymb;

    /**
     * @ORM\ManyToMany(targetEntity=BassinHydrographique::class, inversedBy="reseauxSessions")
     * @ORM\JoinTable(name="RESEAU_SESSION",
     *  joinColumns={@ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")}
     * )
     */
    private $bassins;

    /**
     * @ORM\ManyToMany(targetEntity=Point::class, inversedBy="reseauxSessions")
     * @ORM\JoinTable(name="RESEAU_POINT_PARAM_SESSION",
     *  joinColumns={@ORM\JoinColumn(name="RESEAU_ID", referencedColumnName="RESEAU_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="POINT_ID", referencedColumnName="POINT_ID")}
     * )
     */
    private $points;

    public function __construct()
    {
        $this->bassins = new ArrayCollection();
        $this->points = new ArrayCollection();
    }

    public function getReseauId(): ?int
    {
        return $this->reseauId;
    }

    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    public function getReseauCode(): ?string
    {
        return $this->reseauCode;
    }

    public function getReseauNom(): ?string
    {
        return $this->reseauNom;
    }

    public function getReseauSymbole(): ?string
    {
        return $this->reseauSymbole;
    }

    public function getReseauCouleur(): ?string
    {
        return $this->reseauCouleur;
    }

    public function getReseauRang(): ?int
    {
        return $this->reseauRang;
    }

    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

    public function getGroupeId(): ?int
    {
        return $this->groupeId;
    }

    public function getReseauSymboleTaille(): ?int
    {
        return $this->reseauSymboleTaille;
    }

    public function getReseauImgSymb(): ?string
    {
        return $this->reseauImgSymb;
    }

    /**
     * @return Collection|BassinHydrographique[]
     */
    public function getBassins(): Collection
    {
        return $this->bassins;
    }

    /**
     * @return Collection|Point[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

}
