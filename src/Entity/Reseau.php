<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reseau
 *
 * @ORM\Table(name="RESEAU", indexes={@ORM\Index(name="RESEAU_BASSIN_FK", columns={"BASSIN_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\ReseauRepository")
 */
class Reseau
{
    /**
     * @var int
     *
     * @ORM\Column(name="RESEAU_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $reseauId;

    /**
     * @var string
     *
     * @ORM\Column(name="RESEAU_CODE", type="string", length=20, nullable=false)
     */
    private $reseauCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RESEAU_NOM", type="string", length=100, nullable=false)
     */
    private $reseauNom = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="RESEAU_SYMBOLE", type="string", length=255, nullable=true)
     */
    private $reseauSymbole;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RESEAU_COULEUR", type="string", length=255, nullable=true)
     */
    private $reseauCouleur;

    /**
     * @var int|null
     *
     * @ORM\Column(name="RESEAU_RANG", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $reseauRang;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="GROUPE_ID", type="integer", nullable=false, options={"default"="-1"})
     */
    private $groupeId = -1;

    /**
     * @var int
     *
     * @ORM\Column(name="RESEAU_SYMBOLE_TAILLE", type="integer", nullable=false, options={"default"="8"})
     */
    private $reseauSymboleTaille = 8;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RESEAU_IMG_SYMB", type="string", length=255, nullable=true)
     */
    private $reseauImgSymb;

    /**
     * @ORM\ManyToOne(targetEntity=BassinHydrographique::class, inversedBy="reseau")
     * @ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")
     */
    private $bassinHydrographique;

    /**
     * @ORM\ManyToOne(targetEntity=ReseauGroupe::class, inversedBy="reseaux")
     * @ORM\JoinColumn(name="GROUPE_ID", referencedColumnName="GROUPE_ID")
     */
    private $reseauGroupe;

    /**
     * @ORM\ManyToMany(targetEntity=Point::class, inversedBy="reseaux")
     * @ORM\JoinTable(name="RESEAU_POINT_PARAM",
     *  joinColumns={@ORM\JoinColumn(name="RESEAU_ID", referencedColumnName="RESEAU_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="POINT_ID", referencedColumnName="POINT_ID")}
     * )
     */
    private $points;

    /**
     * @ORM\ManyToMany(targetEntity=Parametre::class, mappedBy="reseaux")
     */
    private $parametres;

    public function __construct()
    {
        $this->points = new ArrayCollection();
        $this->parametres = new ArrayCollection();
    }

    /**
     * Retourne "RESEAU_ID"
     *
     * @return integer|null
     */
    public function getReseauId(): ?int
    {
        return $this->reseauId;
    }

    /**
     * Retourne "RESEAU_CODE"
     *
     * @return string|null
     */
    public function getReseauCode(): ?string
    {
        return $this->reseauCode;
    }

    /**
     * Retourne "RESEAU_NOM"
     *
     * @return string|null
     */
    public function getReseauNom(): ?string
    {
        return $this->reseauNom;
    }

    /**
     * Retourne "RESEAU_SYMBOLE"
     *
     * @return string|null
     */
    public function getReseauSymbole(): ?string
    {
        return $this->reseauSymbole;
    }

    /**
     * Retourne "RESEAU_COULEUR"
     *
     * @return string|null
     */
    public function getReseauCouleur(): ?string
    {
        return $this->reseauCouleur;
    }

    /**
     * Retourne "RESEAU_RANG"
     *
     * @return integer|null
     */
    public function getReseauRang(): ?int
    {
        return $this->reseauRang;
    }

    /**
     * Retourne "BASSIN_ID"
     *
     * @return integer|null
     */
    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

    /**
     * Retourne "GROUPE_ID"
     *
     * @return integer|null
     */
    public function getGroupeId(): ?int
    {
        return $this->groupeId;
    }

    /**
     * Retourne "RESEAU_SYMBOLE_TAILLE"
     *
     * @return integer|null
     */
    public function getReseauSymboleTaille(): ?int
    {
        return $this->reseauSymboleTaille;
    }

    /**
     * Retourne "RESEAU_IMG_SYMB"
     *
     * @return string|null
     */
    public function getReseauImgSymb(): ?string
    {
        return $this->reseauImgSymb;
    }

    /**
     * @return BassinHydrographique|null
     */
    public function getBassinHydrographique(): ?BassinHydrographique
    {
        return $this->bassinHydrographique;
    }

    public function getReseauGroupe(): ?ReseauGroupe
    {
        return $this->reseauGroupe;
    }

    /**
     * @return Collection|Point[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    /**
     * @return Collection|Parametre[]
     */
    public function getParametres(): Collection
    {
        return $this->parametres;
    }

}
