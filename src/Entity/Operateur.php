<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Operateur
 *
 * @ORM\Table(name="OPERATEUR", indexes={@ORM\Index(name="OPERATEUR_BASSIN_FK", columns={"BASSIN_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\OperateurRepository")
 */
class Operateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="OPERATEUR_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $operateurId;

    /**
     * @var string
     *
     * @ORM\Column(name="OPERATEUR_NOM", type="string", length=100, nullable=false)
     */
    private $operateurNom = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="OPERATEUR_CODE", type="string", length=20, nullable=true)
     */
    private $operateurCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;

    /**
     * Retourne "OPERATEUR_ID"
     *
     * @return integer|null
     */
    public function getOperateurId(): ?int
    {
        return $this->operateurId;
    }

    /**
     * Retourne "OPERATEUR_NOM"
     *
     * @return string|null
     */
    public function getOperateurNom(): ?string
    {
        return $this->operateurNom;
    }

    /**
     * Retourne "OPERATEUR_CODE"
     *
     * @return string|null
     */
    public function getOperateurCode(): ?string
    {
        return $this->operateurCode;
    }

    /**
     * Retourne "BASSIN_ID"
     *
     * @return integer|null
     */
    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

}
