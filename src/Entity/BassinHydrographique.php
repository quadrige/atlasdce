<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BassinHydrographique
 *
 * @ORM\Table(name="BASSIN_HYDROGRAPHIQUE")
 * @ORM\Entity(repositoryClass="App\Repository\BassinHydrographiqueRepository")
 */
class BassinHydrographique
{

    /**
     * Liste des codes bassins des outremers
     */
    const CODE_BASSINS_OUTREMERS = [
        'RUN',
        'MAY',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bassinId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="BASSIN_CODE", type="string", length=10, nullable=false)
     */
    private $bassinCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="BASSIN_NOM", type="string", length=255, nullable=false)
     */
    private $bassinNom = '';

    /**
     * @var float
     *
     * @ORM\Column(name="BASSIN_XMIN", type="float", precision=10, scale=0, nullable=false)
     */
    private $bassinXmin = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="BASSIN_YMIN", type="float", precision=10, scale=0, nullable=false)
     */
    private $bassinYmin = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="BASSIN_XMAX", type="float", precision=10, scale=0, nullable=false)
     */
    private $bassinXmax = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="BASSIN_YMAX", type="float", precision=10, scale=0, nullable=false)
     */
    private $bassinYmax = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="BASSIN_ALERT", type="text", length=65535, nullable=true)
     */
    private $bassinAlert;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BASSIN_ALERT_CARTE", type="text", length=65535, nullable=true)
     */
    private $bassinAlertCarte;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BASSIN_URL_WMS", type="string", length=255, nullable=true)
     */
    private $bassinUrlWms;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BASSIN_LAYER_WMS", type="string", length=255, nullable=true)
     */
    private $bassinLayerWms;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BASSIN_PROJECTION", type="string", length=50, nullable=true)
     */
    private $bassinProjection;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_MAJ", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $dateMaj = '0000-00-00 00:00:00';

    /**
     * @ORM\OneToMany(targetEntity=MasseEau::class, mappedBy="bassinHydrographique")
     * @ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")
     */
    private $massesEau;

    /**
     * @ORM\OneToMany(targetEntity=ElementQualite::class, mappedBy="bassinHydrographique")
     * @ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")
     */
    private $elementQualite;

    /**
     * @ORM\OneToMany(targetEntity=EtatMotif::class, mappedBy="bassinHydrographique")
     * @ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")
     */
    private $etatMotif;

    /**
     * @ORM\OneToMany(targetEntity=Reseau::class, mappedBy="bassinHydrographique")
     *
     */
    private $reseaux;

    /**
     * @ORM\ManyToMany(targetEntity=ElementQualiteSession::class, inversedBy="bassinsHydrographiques")
     * @ORM\JoinTable(name="ELEMENT_QUALITE_SESSION",
     *  joinColumns={@ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")}
     * )
     */
    private $elementQualiteSession;

    /**
     * @ORM\ManyToMany(targetEntity=ReseauSession::class, mappedBy="bassins")
     */
    private $reseauxSessions;

    public function __construct()
    {
        $this->massesEau = new ArrayCollection();
        $this->elementQualite = new ArrayCollection();
        $this->etatMotif = new ArrayCollection();
        $this->reseaux = new ArrayCollection();
        $this->elementQualiteSession = new ArrayCollection();
        $this->reseauxSessions = new ArrayCollection();
    }

    /**
     * Retourne "BASSIN_ID"
     *
     * @return integer|null
     */
    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

    /**
     * Retourne "BASSIN_CODE"
     *
     * @return string|null
     */
    public function getBassinCode(): ?string
    {
        return $this->bassinCode;
    }

    /**
     * Retourne "BASSIN_NOM"
     *
     * @return string|null
     */
    public function getBassinNom(): ?string
    {
        return $this->bassinNom;
    }

    /**
     * Retourne "BASSIN_XMIN"
     *
     * @return float|null
     */
    public function getBassinXmin(): ?float
    {
        return $this->bassinXmin;
    }

    /**
     * Retourne "BASSIN_YMIN"
     *
     * @return float|null
     */
    public function getBassinYmin(): ?float
    {
        return $this->bassinYmin;
    }

    /**
     * Retourne "BASSIN_XMAX"
     *
     * @return float|null
     */
    public function getBassinXmax(): ?float
    {
        return $this->bassinXmax;
    }

    /**
     * Retourne "BASSIN_YMAX"
     *
     * @return float|null
     */
    public function getBassinYmax(): ?float
    {
        return $this->bassinYmax;
    }

    /**
     * Retourne "BASSIN_ALERT"
     *
     * @return string|null
     */
    public function getBassinAlert(): ?string
    {
        return $this->bassinAlert;
    }

    /**
     * Retourne "BASSIN_ALERTE_CARTE"
     *
     * @return string|null
     */
    public function getBassinAlertCarte(): ?string
    {
        return $this->bassinAlertCarte;
    }

    /**
     * Retourne BASSIN_URL_WMS
     *
     * @return string|null
     */
    public function getBassinUrlWms(): ?string
    {
        return $this->bassinUrlWms;
    }

    /**
     * Retourne "BASSIN_LAYER_WMS"
     *
     * @return string|null
     */
    public function getBassinLayerWms(): ?string
    {
        return $this->bassinLayerWms;
    }

    /**
     * Retourne "BASSIN_PROJECTION"
     *
     * @return string|null
     */
    public function getBassinProjection(): ?string
    {
        return $this->bassinProjection;
    }

    /**
     * Retourne DATE_MAJ
     *
     * @return \DateTimeInterface|null
     */
    public function getDateMaj(): ?\DateTimeInterface
    {
        return $this->dateMaj;
    }

    /**
     * @return Collection|MasseEau[]
     */
    public function getMassesEau(): Collection
    {
        return $this->massesEau;
    }

    /**
     * @return Collection|ElementQualite[]
     */
    public function getElementQualite(): Collection
    {
        return $this->elementQualite;
    }

    /**
     * @return Collection|EtatMotif[]
     */
    public function getEtatMotif(): Collection
    {
        return $this->etatMotif;
    }

    /**
     * @return Collection|Reseau[]
     */
    public function getReseaux(): Collection
    {
        return $this->reseaux;
    }

    /**
     * @return Collection|ElementQualiteSession[]
     */
    public function getElementQualiteSession(): Collection
    {
        return $this->elementQualiteSession;
    }

    /**
     * @return Collection|ReseauSession[]
     */
    public function getReseauxSessions(): Collection
    {
        return $this->reseauxSessions;
    }

}
