<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassementMasseEau
 *
 * @ORM\Table(name="CLASSEMENT_MASSE_EAU", indexes={@ORM\Index(name="CLASSEMENT_TYPECLASSEMENT_FK", columns={"TYPE_CLASSEMENT_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_SESSION_FK", columns={"SESSION_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_STATUT_FK", columns={"STATUT_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_ELEMENT_FK", columns={"ELEMENT_QUALITE_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_MASSE_FK", columns={"MASSE_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_ETAT_FK", columns={"ETAT_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_MOTIF_FK", columns={"MOTIF_ID"}), @ORM\Index(name="CLASSEMENT_MASSE_EAU_TYPE_ELEMENT_FK", columns={"TYPE_ELEMENT_QUALITE_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\ClassementMasseEauRepository")
 */
class ClassementMasseEau
{
    /**
     * @var int
     *
     * @ORM\Column(name="CLASSEMENT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $classementId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $elementQualiteId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TYPE_ELEMENT_QUALITE_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $typeElementQualiteId;

    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $masseId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="SESSION_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $sessionId = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="MOTIF_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $motifId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="STATUT_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $statutId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ETAT_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $etatId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_DATE", type="datetime", nullable=true, options={"default"="0000-00-00 00:00:00"})
     */
    private $classementDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_DATE_CREA", type="datetime", nullable=true)
     */
    private $classementDateCrea;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_DATE_MAJ", type="datetime", nullable=true)
     */
    private $classementDateMaj;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CLASSEMENT_BILAN", type="text", length=65535, nullable=true)
     */
    private $classementBilan;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CLASSEMENT_COMPLEMENT_BILAN", type="string", length=255, nullable=true)
     */
    private $classementComplementBilan;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CLASSEMENT_DOC_REF", type="string", length=255, nullable=true)
     */
    private $classementDocRef;

    /**
     * @var int|null
     *
     * @ORM\Column(name="CLASSEMENT_INDICE_CONFIANCE", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $classementIndiceConfiance;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $typeClassementId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CLASSEMENT_ME_DATE_MAJ", type="datetime", nullable=true)
     */
    private $classementMeDateMaj;

    public function getClassementId(): ?int
    {
        return $this->classementId;
    }

    public function getElementQualiteId(): ?int
    {
        return $this->elementQualiteId;
    }

    public function getTypeElementQualiteId(): ?int
    {
        return $this->typeElementQualiteId;
    }

    public function getMasseId(): ?int
    {
        return $this->masseId;
    }

    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    public function getMotifId(): ?int
    {
        return $this->motifId;
    }

    public function getStatutId(): ?int
    {
        return $this->statutId;
    }

    public function getEtatId(): ?int
    {
        return $this->etatId;
    }

    public function getClassementDate(): ?\DateTimeInterface
    {
        return $this->classementDate;
    }

    public function getClassementDateCrea(): ?\DateTimeInterface
    {
        return $this->classementDateCrea;
    }

    public function getClassementDateMaj(): ?\DateTimeInterface
    {
        return $this->classementDateMaj;
    }

    public function getClassementBilan(): ?string
    {
        return $this->classementBilan;
    }

    public function getClassementComplementBilan(): ?string
    {
        return $this->classementComplementBilan;
    }

    public function getClassementDocRef(): ?string
    {
        return $this->classementDocRef;
    }

    public function getClassementIndiceConfiance(): ?int
    {
        return $this->classementIndiceConfiance;
    }

    public function getTypeClassementId(): ?int
    {
        return $this->typeClassementId;
    }

    public function getClassementMeDateMaj(): ?\DateTimeInterface
    {
        return $this->classementMeDateMaj;
    }

}
