<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ElementQualite
 *
 * @ORM\Table(name="ELEMENT_QUALITE", indexes={@ORM\Index(name="ELEMENT_QUALITE_TYPE_ELT_FK", columns={"TYPE_ELEMENT_QUALITE_ID"}), @ORM\Index(name="ELEMENT_QUALITE_TYPE_CLASSEMENT_FK", columns={"TYPE_CLASSEMENT_ID"}), @ORM\Index(name="ELEMENT_BASSIN_FK", columns={"BASSIN_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\ElementQualiteRepository")
 */
class ElementQualite
{
    /**
     * @var int
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $elementQualiteId;

    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_CLASSEMENT_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $typeClassementId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="TYPE_ELEMENT_QUALITE_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $typeElementQualiteId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ELEMENT_QUALITE_NOM", type="string", length=255, nullable=false)
     */
    private $elementQualiteNom = '';

    /**
     * @var int
     *
     * @ORM\Column(name="ELEMENT_QUALITE_NIVEAU", type="integer", nullable=false, options={"default"="1","unsigned"=true})
     */
    private $elementQualiteNiveau = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="ELEMENT_QUALITE_PARENT", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $elementQualiteParent = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_ARBRE", type="string", length=20, nullable=true)
     */
    private $elementQualiteArbre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_RANG", type="integer", nullable=true)
     */
    private $elementQualiteRang;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_URL_FICHE", type="string", length=255, nullable=true)
     */
    private $elementQualiteUrlFiche;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_DOC_PROTOCOLE", type="string", length=255, nullable=true)
     */
    private $elementQualiteDocProtocole;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_DOC_METHODO", type="string", length=255, nullable=true)
     */
    private $elementQualiteDocMethodo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ELEMENT_QUALITE_RESTRICT_TYPEME", type="string", length=10, nullable=true)
     */
    private $elementQualiteRestrictTypeme;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=true, options={"default"="1","unsigned"=true})
     */
    private $bassinId = 1;

    /**
     * @ORM\ManyToOne(targetEntity=BassinHydrographique::class, inversedBy="elementQualite")
     * @ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")
     */
    private $bassinHydrographique;

    /**
     * @ORM\ManyToOne(targetEntity=TypeElementQualite::class, inversedBy="elementQualites")
     * @ORM\JoinColumn(name="TYPE_ELEMENT_QUALITE_ID", referencedColumnName="TYPE_ELEMENT_QUALITE_ID")
     */
    private $typeElementQualite;

    /**
     * @ORM\ManyToOne(targetEntity=TypeClassement::class, inversedBy="elementQualites")
     * @ORM\JoinColumn(name="TYPE_CLASSEMENT_ID", referencedColumnName="TYPE_CLASSEMENT_ID")
     */
    private $typeClassement;

    /**
     * @ORM\ManyToMany(targetEntity=MasseEau::class, mappedBy="elementQualite")
     */
    private $massesEau;

    public function __construct()
    {
        $this->massesEau = new ArrayCollection();
    }

    public function getBassinHydrographique(): ?BassinHydrographique
    {
        return $this->bassinHydrographique;
    }

    /**
     * Retourne "ELEMENT_QUALITE_ID"
     *
     * @return integer|null
     */
    public function getElementQualiteId(): ?int
    {
        return $this->elementQualiteId;
    }

    /**
     * Retourne "TYPE_CLASSEMENT_ID"
     *
     * @return integer|null
     */
    public function getTypeClassementId(): ?int
    {
        return $this->typeClassementId;
    }

    /**
     * Retourne "TYPE_ELEMENT_QUALITE_ID"
     *
     * @return integer|null
     */
    public function getTypeElementQualiteId(): ?int
    {
        return $this->typeElementQualiteId;
    }

    /**
     * Retourne "ELEMENT_QUALITE_NOM"
     *
     * @return string|null
     */
    public function getElementQualiteNom(): ?string
    {
        return $this->elementQualiteNom;
    }

    /**
     * Retourne "ELEMENT_QUALITE_NIVEAU"
     *
     * @return integer|null
     */
    public function getElementQualiteNiveau(): ?int
    {
        return $this->elementQualiteNiveau;
    }

    /**
     * Retourne "ELEMENT_QUALITE_PARENT"
     *
     * @return integer|null
     */
    public function getElementQualiteParent(): ?int
    {
        return $this->elementQualiteParent;
    }

    /**
     * Retourne "ELEMENT_QUALITE_ARBRE"
     *
     * @return string|null
     */
    public function getElementQualiteArbre(): ?string
    {
        return $this->elementQualiteArbre;
    }

    /**
     * Retourne "ELEMENT_QUALITE_RANG"
     *
     * @return integer|null
     */
    public function getElementQualiteRang(): ?int
    {
        return $this->elementQualiteRang;
    }

    /**
     * Retourne "ELEMENT_QUALITE_URL_FICHE"
     *
     * @return string|null
     */
    public function getElementQualiteUrlFiche(): ?string
    {
        return $this->elementQualiteUrlFiche;
    }

    /**
     * Retourne "ELEMENT_QUALITE_DOC_PROTOCOLE"
     *
     * @return string|null
     */
    public function getElementQualiteDocProtocole(): ?string
    {
        return $this->elementQualiteDocProtocole;
    }

    /**
     * Retourne "ELEMENT_QUALITE_DOC_METHODO"
     *
     * @return string|null
     */
    public function getElementQualiteDocMethodo(): ?string
    {
        return $this->elementQualiteDocMethodo;
    }

    /**
     * Retourne "ELEMENT_QUALITE_RESTRICT_TYPEME"
     *
     * @return string|null
     */
    public function getElementQualiteRestrictTypeme(): ?string
    {
        return $this->elementQualiteRestrictTypeme;
    }

    /**
     * Retourne "BASSIN_ID"
     *
     * @return integer|null
     */
    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

    /**
     * @return TypeElementQualite|null
     */
    public function getTypeElementQualite(): ?TypeElementQualite
    {
        return $this->typeElementQualite;
    }

    /**
     * @return TypeClassement|null
     */
    public function getTypeClassement(): ?TypeClassement
    {
        return $this->typeClassement;
    }

    /**
     * @return Collection|MasseEau[]
     */
    public function getMassesEau(): Collection
    {
        return $this->massesEau;
    }

}
