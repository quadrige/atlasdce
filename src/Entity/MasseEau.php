<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MasseEau
 *
 * @ORM\Table(name="MASSE_EAU", indexes={@ORM\Index(name="MASSE_EAU_BASSIN_FK", columns={"BASSIN_ID"}), @ORM\Index(name="ME_ATTEINTE_OBJ_FK", columns={"MASSE_B_ATTEINTE_OBJ"}), @ORM\Index(name="MASSE_EAU_DESC_FK", columns={"MASSE_DESC_ID"}), @ORM\Index(name="MASSE_EAU_REGION_FK", columns={"REGION_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\MasseEauRepository")
 */
class MasseEau
{
    /**
     * @var int
     *
     * @ORM\Column(name="MASSE_ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $masseId;

    /**
     * @var string
     *
     * @ORM\Column(name="MASSE_CODE", type="string", length=20, nullable=false)
     */
    private $masseCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MASSE_NOM", type="string", length=255, nullable=false)
     */
    private $masseNom = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_TYPE", type="string", length=45, nullable=true)
     */
    private $masseType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_TYPE_SUIVI", type="string", length=45, nullable=true)
     */
    private $masseTypeSuivi;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBSuivi;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_RISQUE", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBRisque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_URL_FICHE", type="string", length=255, nullable=true)
     */
    private $masseUrlFiche;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_DESC_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseDescId;

    /**
     * @var int
     *
     * @ORM\Column(name="BASSIN_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $bassinId = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="REGION_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $regionId = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_RANG_GEO", type="integer", nullable=true)
     */
    private $masseRangGeo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI_OP", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBSuiviOp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_ATTEINTE_OBJECTIF", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBAtteinteObjectif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_OBJECTIF", type="string", length=255, nullable=true)
     */
    private $masseObjectif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_REPORT_OBJECTIF", type="string", length=255, nullable=true)
     */
    private $masseReportObjectif;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_FORT_MODIF", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $masseBFortModif;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="MASSE_DATE_FORT_MODIF", type="datetime", nullable=true)
     */
    private $masseDateFortModif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_DOC_DESC", type="string", length=255, nullable=true)
     */
    private $masseDocDesc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI_ENQUETE", type="integer", nullable=true)
     */
    private $masseBSuiviEnquete;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_SUIVI_SURVEILLANCE", type="integer", nullable=true)
     */
    private $masseBSuiviSurveillance;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_COMMENTAIRE", type="text", length=65535, nullable=true)
     */
    private $masseCommentaire;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MASSE_B_ATTEINTE_OBJ", type="integer", nullable=true)
     */
    private $masseBAtteinteObj;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_MOTIF_DEROGATION", type="string", length=100, nullable=true)
     */
    private $masseMotifDerogation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MASSE_DOC_FICHE", type="string", length=255, nullable=true)
     */
    private $masseDocFiche;

    /**
     * @ORM\ManyToOne(targetEntity=MasseEauDescription::class, inversedBy="massesEau")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="MASSE_DESC_ID", referencedColumnName="MASSE_DESC_ID")})
     * 
     */
    private $masseEauDescription;

    /**
     * @ORM\ManyToOne(targetEntity=BassinHydrographique::class, inversedBy="massesEau")
     * @ORM\JoinColumn(name="BASSIN_ID", referencedColumnName="BASSIN_ID")
     */
    private $bassinHydrographique;

    /**
     * @ORM\ManyToOne(targetEntity=RegionMarine::class, inversedBy="massesEau")
     * @ORM\JoinColumn(name="REGION_ID", referencedColumnName="REGION_ID")
     */
    private $regionMarine;

    /**
     * @ORM\ManyToMany(targetEntity=Departement::class, inversedBy="massesEau")
     * @ORM\JoinTable(name="MASSE_EAU_DEPT",
     *   joinColumns={@ORM\JoinColumn(name="MASSE_ID", referencedColumnName="MASSE_ID")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="DEPT_ID", referencedColumnName="DEPT_ID")}
     * )
     */
    private $departement;

    /**
     * @ORM\ManyToOne(targetEntity=AtteinteObjectifDescription::class, inversedBy="massesEau")
     * @ORM\JoinColumn(name="MASSE_B_ATTEINTE_OBJ", referencedColumnName="ATTEINTE_OBJECTIF_ID")
     */
    private $atteinteObjectifDescription;

    /**
     * @ORM\OneToMany(targetEntity=Point::class, mappedBy="masseEau")
     */
    private $points;

    /**
     * @ORM\ManyToMany(targetEntity=Etat::class, inversedBy="massesEau")
     * @ORM\JoinTable(name="CLASSEMENT_MASSE_EAU",
     *   joinColumns={@ORM\JoinColumn(name="MASSE_ID", referencedColumnName="MASSE_ID")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="ETAT_ID", referencedColumnName="ETAT_ID")}
     * )
     */
    private $etats;

    /**
     * @ORM\ManyToMany(targetEntity=ElementQualite::class, inversedBy="massesEau")
     * @ORM\JoinTable(name="CLASSEMENT_MASSE_EAU",
     *   joinColumns={@ORM\JoinColumn(name="MASSE_ID", referencedColumnName="MASSE_ID")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="ELEMENT_QUALITE_ID", referencedColumnName="ELEMENT_QUALITE_ID")}
     * )
     */
    private $elementQualite;

    public function __construct()
    {
        $this->departement = new ArrayCollection();
        $this->points = new ArrayCollection();
        $this->etats = new ArrayCollection();
        $this->elementQualite = new ArrayCollection();
    }

    /**
     * Retourne "MASSE_ID"
     *
     * @return integer|null
     */
    public function getMasseId(): ?int
    {
        return $this->masseId;
    }

    /**
     * Retourne "MASSE_CODE"
     *
     * @return string|null
     */
    public function getMasseCode(): ?string
    {
        return $this->masseCode;
    }

    /**
     * Retourne "MASSE_NOM"
     *
     * @return string|null
     */
    public function getMasseNom(): ?string
    {
        return $this->masseNom;
    }

    /**
     * Retourne "MASSE_TYPE"
     */
    public function getMasseType(): ?string
    {
        return $this->masseType;
    }

    /**
     * Retourne "MASSE_TYPE_SUIVI"
     *
     * @return string|null
     */
    public function getMasseTypeSuivi(): ?string
    {
        return $this->masseTypeSuivi;
    }

    /**
     * Retourne "MASSE_B_SUIVI"
     *
     * @return boolean|null
     */
    public function getMasseBSuivi(): ?bool
    {
        return $this->masseBSuivi ? true : false;
    }

    /**
     * Retourne "MASSE_B_RISQUE"
     *
     * @return boolean|null
     */
    public function getMasseBRisque(): ?bool
    {
        return $this->masseBRisque ? true : false;
    }

    /**
     * Retourne "MASSE_URL_FICHE"
     *
     * @return string|null
     */
    public function getMasseUrlFiche(): ?string
    {
        return $this->masseUrlFiche;
    }

    /**
     * Retourne "MASSE_DESC_ID"
     *
     * @return integer|null
     */
    public function getMasseDescId(): ?int
    {
        return $this->masseDescId;
    }

    /**
     * Retourne "BASSIN_ID"
     *
     * @return integer|null
     */
    public function getBassinId(): ?int
    {
        return $this->bassinId;
    }

    /**
     * Retourne "REGION_ID"
     *
     * @return integer|null
     */
    public function getRegionId(): ?int
    {
        return $this->regionId;
    }

    /**
     * Retourne "MASSE_RANG_GEO"
     *
     * @return integer|null
     */
    public function getMasseRangGeo(): ?int
    {
        return $this->masseRangGeo;
    }

    /**
     * Retourne "MASSE_B_SUIVI_OP"
     *
     * @return boolean|null
     */
    public function getMasseBSuiviOp(): ?bool
    {
        return $this->masseBSuiviOp ? true : false;
    }

    /**
     * Retourne "MASSE_B_ATTEINTE_OBJECTIF"
     *
     * @return integer|null
     */
    public function getMasseBAtteinteObjectif(): ?int
    {
        return $this->masseBAtteinteObjectif;
    }

    /**
     * Retourne "MASSE_OBJECTIF"
     *
     * @return string|null
     */
    public function getMasseObjectif(): ?string
    {
        return $this->masseObjectif;
    }

    /**
     * Retourne "MASSE_REPORT_OBJECTIF"
     *
     * @return string|null
     */
    public function getMasseReportObjectif(): ?string
    {
        return $this->masseReportObjectif;
    }

    /**
     * Retourne "MASSE_B_FORT_MODIF"
     *
     * @return boolean|null
     */
    public function getMasseBFortModif(): ?bool
    {
        return $this->masseBFortModif ? true : false;
    }

    /**
     * Retourne "MASSE_DATE_FORT_MODIF"
     *
     * @return \DateTimeInterface|null
     */
    public function getMasseDateFortModif(): ?\DateTimeInterface
    {
        return $this->masseDateFortModif;
    }

    /**
     * Retourne "MASSE_DOC_DESC"
     *
     * @return string|null
     */
    public function getMasseDocDesc(): ?string
    {
        return $this->masseDocDesc;
    }

    /**
     * Retourne "MASSE_B_SUIVI_ENQUETE"
     *
     * @return integer|null
     */
    public function getMasseBSuiviEnquete(): ?int
    {
        return $this->masseBSuiviEnquete;
    }

    /**
     * "MASSE_B_SUIVI_SURVEILLANCE"
     *
     * @return integer|null
     */
    public function getMasseBSuiviSurveillance(): ?int
    {
        return $this->masseBSuiviSurveillance;
    }

    /**
     * Retourne "MASSE_COMMENTAIRE"
     *
     * @return string|null
     */
    public function getMasseCommentaire(): ?string
    {
        return $this->masseCommentaire;
    }

    /**
     * Retourne "MASSE_B_ATTEINTE_OBJ"
     *
     * @return boolean|null
     */
    public function getMasseBAtteinteObj(): ?bool
    {
        return $this->masseBAtteinteObj ? true : false;
    }

    /**
     * Retourne "MASSE_MOTIF_DEROGATION"
     *
     * @return string|null
     */
    public function getMasseMotifDerogation(): ?string
    {
        return $this->masseMotifDerogation;
    }

    /**
     * Retourne "MASSE_DOC_FICHE"
     *
     * @return string|null
     */
    public function getMasseDocFiche(): ?string
    {
        return $this->masseDocFiche;
    }

    /**
     * @return MasseEauDescription|null
     */
    public function getMasseEauDescription(): ?MasseEauDescription
    {
        return $this->masseEauDescription;
    }

    /**
     * @return BassinHydrographique|null
     */
    public function getBassinHydrographique(): ?BassinHydrographique
    {
        return $this->bassinHydrographique;
    }

    /**
     * @return RegionMarine|null
     */
    public function getRegionMarine(): ?RegionMarine
    {
        return $this->regionMarine;
    }

    /**
     * @return Collection|Departement[]
     */
    public function getDepartement(): Collection
    {
        return $this->departement;
    }

    /**
     * @return AtteinteObjectifDescription|null
     */
    public function getAtteinteObjectifDescription(): ?AtteinteObjectifDescription
    {
        return $this->atteinteObjectifDescription;
    }

    /**
     * @return Collection|Point[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    /**
     * @return Collection|Etat[]
     */
    public function getEtats(): Collection
    {
        return $this->etats;
    }

    /**
     * @return Collection|ElementQualite[]
     */
    public function getElementQualite(): Collection
    {
        return $this->elementQualite;
    }

}
