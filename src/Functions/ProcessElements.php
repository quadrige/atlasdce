<?php

namespace App\Functions;

use function _\groupBy;

class ProcessElements
{
    private $elements;

    public function __construct(array $elements)
    {
        $this->elements = $elements;
    }

    /**
     * Place les éléments 'enfants' dans la propriété 'children' des élements parents
     *
     * @return array
     */
    public function unifiedElements(): array
    {
        $parentsElement = [];
        $childrenElement = [];
        $unifiedElement = [];

        foreach ($this->elements as $element) {
            if ($element['parent'] !== 0) {
                $childrenElement[] = $element; 
            } else {
                $parentsElement[] = $element; 
            }
        }

        $childrenGroup = groupBy($childrenElement, 'parent');

        foreach ($parentsElement as $parent) {
            if (array_key_exists($parent['id'], $childrenGroup) && $childrenGroup[$parent['id']]) {
                $parent['children'] = $childrenGroup[$parent['id']];
            }

            $unifiedElement[] = $parent;
        }

        return $unifiedElement;
    }
}
