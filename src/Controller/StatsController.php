<?php

namespace App\Controller;

use App\Repository\BassinHydrographiqueRepository;
use App\Repository\MasseEauRepository;
use App\Repository\ParametreRepository;
use App\Repository\PointRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\PersistentCollection;

class StatsController extends AbstractController
{
    /**
     * Retourne les chiffres clés
     * 
     * @Route("/stats", name="stats", methods={"GET"})
     *
     * @param MasseEauRepository $masseEauRepository
     * @param PointRepository $pointRepository
     * @param ParametreRepository $parametreRepository
     * @param BassinHydrographiqueRepository $bassinHydrographiqueRepository
     * @return Response
     */
    public function count(
        MasseEauRepository $masseEauRepository,
        PointRepository $pointRepository,
        ParametreRepository $parametreRepository,
        BassinHydrographiqueRepository $bassinHydrographiqueRepository): Response
    {
        $countedMasseType = $masseEauRepository->countMasseType();
        $countedPoint = $pointRepository->countPoint();
        $countedParametre = $parametreRepository->countParametre();
        $countedBassin = $bassinHydrographiqueRepository->countBassin();

        return $this->json([
            'bassinsMetropole' => $countedBassin['bassinsMetropole'],
            'bassinsOutremers' => $countedBassin['bassinsOutremers'],
            'massesCotieres' => $countedMasseType['cotiere'],
            'massesTransitions' => $countedMasseType['transition'],
            'points' => $countedPoint,
            'parametres' => $countedParametre
        ]);
    }

    /**
     * Retourne les chiffres clés en fonction du code du bassin
     * 
     * @Route("/stats/{bassinCode}", name="bassin_stats", methods={"GET"})
     *
     * @param string $bassinCode
     * @param BassinHydrographiqueRepository $bassinHydrographiqueRepository
     * @param MasseEauRepository $masseEauRepository
     * @param PointRepository $pointRepository
     * @return Response
     */
    public function countByBassin(
        string $bassinCode,
        BassinHydrographiqueRepository $bassinHydrographiqueRepository,
        MasseEauRepository $masseEauRepository,
        PointRepository $pointRepository): Response
    {
        $bassin = $bassinHydrographiqueRepository->findOneBy(['bassinCode' => $bassinCode]);

        if (!$bassin) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Non trouvé'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        $bassinId = $bassin->getBassinId();
        $countMasses = $masseEauRepository->countMassTypeByBassinId($bassinId);
        $massesByBassinId = $masseEauRepository->findBy(['bassinId' => $bassinId]);
        $massesEauIds = $this->normalizeMassesEau($massesByBassinId);
        $countPoint = $pointRepository->countPointByMassesEau($massesEauIds);
        $reseaux = $bassin->getReseaux();
        $countParametre = $this->countParameter($reseaux);

        return $this->json([
            'massesCotieres' => $countMasses['cotiere'],
            'massesTransitions' => $countMasses['transition'],
            'points' => $countPoint,
            'parametres' => $countParametre
        ]);
    }

    private function normalizeMassesEau(array $massesEau): array
    {
        foreach ($massesEau as $masseEau) {
            $massesEauResult[] = $masseEau->getMasseId();
        }

        return $massesEauResult;
    }

    private function countParameter(PersistentCollection $reseaux): int
    {
        $parametresResult = [];
        
        foreach ($reseaux as $result) {
            $parametres = $result->getParametres();
            foreach ($parametres as $parametre) {
                $parametresResult[] = [
                    'id' => $parametre->getParametreId(),
                    'code' => $parametre->getParametreCode(),
                    'nom' => $parametre->getParametreNom()
                ];
            }

        }

        return count($parametresResult);
    }
}
