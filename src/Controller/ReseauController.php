<?php

namespace App\Controller;

use App\Entity\Reseau;
use App\Repository\ReseauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReseauController extends AbstractController
{
    /**
     * Retourne le contenu de tous les reseaux
     * 
     * @Route("/reseaux", name="reseaux", methods={"GET"})
     *
     * @param ReseauRepository $reseauRepository
     * @return Response
     */
    public function showAll(ReseauRepository $reseauRepository): Response
    {
        $reseaux = $reseauRepository->findAll();

        if (!$reseaux) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Non trouvé'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        foreach ($reseaux as $reseau) {
            $reseauxResult[] = $this->reseauExport($reseau);
        }

        $response = $this->json($reseauxResult);
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);

        return $response;
    }

    /**
     * Retourne le contenu d'un reseau en fonction de son id
     * 
     * @Route("/reseaux/{id}", name="reseau", methods={"GET"})
     *
     * @param integer $id
     * @param ReseauRepository $reseauRepository
     * @return Response
     */
    public function show(int $id, ReseauRepository $reseauRepository): Response
    {
        $reseau = $reseauRepository->find($id);

        if (!$reseau) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de réseau ayant pour id ' . $id
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json($this->reseauExport($reseau));
    }

    /**
     * Retourne le contenu d'un reseau en fonction de son code et de l'id du bassin
     * 
     * @Route("/reseaux/{reseauCode}/bassin/{bassinId}", name="reseau-code-bassin", methods={"GET"})
     *
     * @param string $reseauCode
     * @param integer $bassinId
     * @param ReseauRepository $reseauRepository
     * @return Response
     */
    public function showByCode(string $reseauCode, int $bassinId, ReseauRepository $reseauRepository): Response
    {
        $reseau = $reseauRepository->findOneBy(['reseauCode' => $reseauCode, 'bassinId' => $bassinId]);

        if (!$reseau) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de réseau ayant pour code ' . $reseauCode
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json($this->reseauExport($reseau));
    }

    /**
     * Retourne une liste de reseau en fonction de l'id du bassin
     * 
     * @Route("/reseaux/bassin/{bassinId}", name="reseau-bassin", methods={"GET"})
     *
     * @param integer $bassinId
     * @param ReseauRepository $reseauRepository
     * @return Response
     */
    public function showByBassinId(int $bassinId, ReseauRepository $reseauRepository): Response
    {
        $reseaux = $reseauRepository->findBy(['bassinId' => $bassinId]);

        if (!$reseaux) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Non trouvé'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        foreach ($reseaux as $reseau) {
            $reseauxResult[] = $this->reseauExport($reseau);
        }

        $response = $this->json($reseauxResult);
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);

        return $response;
    }

    /**
     * Retourne le contenu d'un reseau sous frome d'un tableau associatif
     *
     * @param Reseau $reseau
     * @return array
     */
    private function reseauExport(Reseau $reseau): array
    {
        $parametres = $reseau->getParametres();
        $parametresResult = [];

        foreach ($parametres as $parametre) {
            $parametresResult[] = [
                'id' => $parametre->getParametreId(),
                'code' => $parametre->getParametreCode(),
                'nom' => $parametre->getParametreNom(),
            ];
        }

        return [
            'id' => $reseau->getReseauId(),
            'code' => $reseau->getReseauCode(),
            'nom' => $reseau->getReseauNom(),
            'symbole' => $reseau->getReseauSymbole() !== '-1' ? $reseau->getReseauSymbole() : null,
            'symboleTaille' => $reseau->getReseauSymboleTaille(),
            'couleur' => $reseau->getReseauCouleur(),
            'imgSymb' => $reseau->getReseauSymbole() === '-1' ? $reseau->getReseauImgSymb() : null,
            'parametres' => $parametresResult
        ];
    }
}
