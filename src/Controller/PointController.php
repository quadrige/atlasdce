<?php

namespace App\Controller;

use App\Entity\Point;
use App\Repository\PointRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PointController extends AbstractController
{
    /**
     * Retourne le contenu de tous les points
     * 
     * @Route("/points", name="points", methods={"GET"})
     *
     * @param PointRepository $pointRepository
     * @return Response
     */
    public function showAll(PointRepository $pointRepository): Response
    {
        $points = $pointRepository->findAll();
        $pointsResult = [];

        if (!$points) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Non trouvé'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        foreach ($points as $point) {
            $reseaux = $this->pointExport($point)['reseaux'];
            if (count($reseaux) > 0 && count($reseaux) < 2) {
                $rawPoint = $this->pointExport($point);
                $rawPoint['idWreseau'] = $rawPoint['id'] . '-' . $rawPoint['reseaux'][0]['id'];
                array_push($pointsResult, $rawPoint);
            } else if (count($reseaux) > 1) {
                $multiplePoints = $this->pointExport($point);
                for ($i = 0; $i < count($reseaux); $i++) {
                    $multiplePoints['idWreseau'] = $multiplePoints['id'] . '-' . $multiplePoints['reseaux'][$i]['id'];
                    array_push($pointsResult, $multiplePoints);
                }
            }
        }

        $response = $this->json($pointsResult);
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);

        return $response;
    }

    /**
     * Retourne le contenu d'un point en fonction de son id
     * 
     * @Route("/points/{id}", name="point", methods={"GET"})
     *
     * @param integer $id
     * @param PointRepository $pointRepository
     * @return Response
     */
    public function show(int $id, PointRepository $pointRepository): Response
    {
        $point = $pointRepository->find($id);

        if (!$point) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de point ayant pour id ' . $id
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json($this->pointExport($point));
    }

    /**
     *
     * Retourne le contenu d'un point en fonction de son code
     *
     * @Route("/points/code/{code}", name="point-code", methods={"GET"})
     *
     * @param string $code
     * @param PointRepository $pointRepository
     * @return Response
     */
    public function showByCode(string $code, PointRepository $pointRepository): Response
    {
        $point = $pointRepository->findOneBy(['pointCode' => $code]);

        if (!$point) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de point ayant pour code ' . $code
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json($this->pointExport($point));
    }

    /**
     * Retourne le contenu d'un point sous frome d'un tableau associatif
     *
     * @param Point $point
     * @return array
     */
    private function pointExport(Point $point): array
    {
        $masseEau = $point->getMasseEau();
        $bassinId = $masseEau->getBassinId();

        return [
            'id' => $point->getPointId(),
            'nom' => $point->getPointNom(),
            'code' => $point->getPointCode(),
            'lon' => (float) $point->getPointLongitude(),
            'lat' => (float) $point->getPointLatitude(),
            'bassinId' => $bassinId,
            'masseId' => $point->getMasseId(),
            'masseCode' => $masseEau->getMasseCode(),
            'masseNom' => $masseEau->getMasseNom(),
            'masseType' => $masseEau->getMasseType(),
            'reseaux' => $this->getReseaux($point),
        ];
    }

    /**
     * Retourne les réseaux en fonction du Point
     *
     * @param Point $point
     * @return array
     */
    private function getReseaux(Point $point): array
    {
        $reseaux = $point->getReseaux();
        $reseauxId = [];

        foreach ($reseaux as $reseau) {
            $reseauId = $reseau->getReseauId();
            $reseauCode = $reseau->getReseauCode();
            $reseauNom = $reseau->getReseauNom();
            $reseauGroupe = $reseau->getReseauGroupe();

            $reseauxId[] = [
                'id' => $reseauId,
                'code' => $reseauCode,
                'nom' => $reseauNom,
                'reseauGroupe' => $reseauGroupe && $reseauGroupe->getGroupeId() !== -1 ? $reseauGroupe->getGroupeNom() : null
            ];
        }

        return $reseauxId;
    }
}
