<?php

namespace App\Controller;

use App\Entity\Parametre;
use App\Repository\ParametreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParametreController extends AbstractController
{
    /**
     * Retourne le contenu de tous les parametres
     * 
     * @Route("/parametres", name="parametres", methods={"GET"})
     *
     * @param ParametreRepository $parametreRepository
     * @return Response
     */
    public function showAll(ParametreRepository $parametreRepository): Response
    {
        $parametres = $parametreRepository->findAll();

        if (!$parametres) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Non trouvé'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        foreach ($parametres as $parametre) {
            $parametresResult[] = $this->parametreExport($parametre);
        }

        return $this->json($parametresResult);
    }

    /**
     * Retourne le contenu d'un parametre en fonction de son id
     * 
     * @Route("/parametres/{id}", name="parametre", methods={"GET"})
     *
     * @param integer $id
     * @param ParametreRepository $parametreRepository
     * @return Response
     */
    public function show(int $id, ParametreRepository $parametreRepository): Response
    {
        $parametre = $parametreRepository->find($id);

        if (!$parametre) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de réseau ayant pour id ' . $id
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json($this->parametreExport($parametre));
    }

    /**
     * Retourne le contenu d'un parametre
     *
     * @param Parametre $parametre
     * @return array
     */
    private function parametreExport(Parametre $parametre): array
    {
        return [
            'id' => $parametre->getParametreId(),
            'code' => $parametre->getParametreCode(),
            'nom' => $parametre->getParametreNom(),
        ];
    }

}
