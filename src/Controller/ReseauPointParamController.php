<?php

namespace App\Controller;

use App\Repository\OperateurRepository;
use App\Repository\ParametreRepository;
use App\Repository\ReseauPointParamRepository;
use App\Repository\ReseauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReseauPointParamController extends AbstractController
{

    /**
     * @Route("/reseau-point-param/{pointId}", name="reseau-point-param", methods={"GET"})
     *
     * @param integer $pointId
     * @param ReseauRepository $reseauRepository
     * @param ReseauPointParamRepository $reseauPointParamRepository
     * @param OperateurRepository $operateurRepository
     * @param ParametreRepository $parametreRepository
     * @return Response
     */
    public function showByPoint(
        int $pointId,
        ReseauRepository $reseauRepository,
        ReseauPointParamRepository $reseauPointParamRepository,
        OperateurRepository $operateurRepository,
        ParametreRepository $parametreRepository): Response
    {
        $reseauPointParam = $reseauPointParamRepository->findReseauxByPointId($pointId);


        return $this->json($this->normalizeReseauPointParam(
            $reseauPointParam,
            $operateurRepository,
            $parametreRepository,
            $reseauRepository
        ));
    }

    /**
     * Retourne ReseauPointParam sous forme de tableau associatif
     *
     * @param array $reseauxPointParam
     * @param OperateurRepository $operateurRepository
     * @param ParametreRepository $parametreRepository
     * @param ReseauRepository $reseauRepository
     * @return array
     */
    private function normalizeReseauPointParam(
        array $reseauxPointParam,
        OperateurRepository $operateurRepository,
        ParametreRepository $parametreRepository,
        ReseauRepository $reseauRepository): array
    {
        $reseauxPointParamResult = [];

        foreach ($reseauxPointParam as $reseauPointParam) {
            $pointId = $reseauPointParam['p_pointId'];
            $parametreId = $reseauPointParam['p_parametreId'];
            $reseauId = $reseauPointParam['p_reseauId'];
            $operateurTerrainId = $reseauPointParam['p_operateurTerrainId'];
            $operateurLaboId = $reseauPointParam['p_operateurLaboId'];
            $frequence = $reseauPointParam['p_frequence'];
            $anneePrelevement = $reseauPointParam['p_anneePrelevement'];
            $frequencePlan = $reseauPointParam['p_frequencePlan'];
            $periode = $reseauPointParam['p_periode'];
            $reseauDateMaj = $reseauPointParam['p_reseauDateMaj'];

            $operateurTerrain = $operateurRepository->find($operateurTerrainId);
            $operateurLabo = $operateurRepository->find($operateurLaboId);
            $parametre = $parametreRepository->find($parametreId);

            if ($reseauRepository->find($reseauId)) {
                $reseauxPointParamResult[] = [
                    'pointId' => $pointId ,
                    'parametre' => $parametre->getParametreNom(),
                    'reseauId' => $reseauId,
                    'operateurTerrain' => $operateurTerrain->getOperateurNom(),
                    'operateurLabo' => $operateurLabo->getOperateurNom(),
                    'frequence' => $frequence,
                    'anneePrelevement' => $anneePrelevement,
                    'frequencePlan' => $frequencePlan,
                    'periode' => $periode,
                    'reseauDateMaj' => $reseauDateMaj
                ];
            }

        }

        return $reseauxPointParamResult;
    }
}
