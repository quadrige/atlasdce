<?php

namespace App\Controller;

use App\Entity\ClassementStatut;
use App\Entity\Etat;
use App\Entity\EtatMotif;
use App\Entity\MasseEau;
use App\Functions\ProcessElements;
use App\Repository\ClassementMasseEauRepository;
use App\Repository\ClassementStatutRepository;
use App\Repository\EtatMotifRepository;
use App\Repository\EtatRepository;
use App\Repository\MasseEauRepository;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function _\countBy;
use function _\groupBy;
use function _\orderBy;

class MasseEauController extends AbstractController
{
    /**
     * Retourne le contenu de toutes les masses d'eau
     * 
     * @Route("/masses-eau", name="masses-eau", methods={"GET"})
     *
     * @param MasseEauRepository $masseEauRepository
     * @return Response
     */
    public function showAll(
        MasseEauRepository $masseEauRepository,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        EtatMotifRepository $etatMotifRepository,
        ClassementStatutRepository $classementStatutRepository): Response
    {
        $massesEau = $masseEauRepository->findAll();

        if (!$massesEau) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Non trouvé'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        foreach ($massesEau as $masseEau) {
            $massesEauResult[] = $this->masseEauLightExport($masseEau, $classementMasseEauRepository, $etatRepository, $etatMotifRepository, $classementStatutRepository);
        }

        $response = $this->json($massesEauResult);
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);

        return $response;
    }

    /**
     * Retourne le contenu d'une masse d'eau en fonction de son id
     * 
     * @Route("/masses-eau/{id}", name="masse-eau", methods={"GET"})
     *
     * @param integer $id
     * @param MasseEauRepository $masseEauRepository
     * @return Response
     */
    public function show(
        int $id,
        MasseEauRepository $masseEauRepository,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        EtatMotifRepository $etatMotifRepository,
        ClassementStatutRepository $classementStatutRepository): Response
    {
        $masseEau = $masseEauRepository->find($id);

        if (!$masseEau) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de masse d’eau ayant pour id ' . $id
                ],
                Response::HTTP_NOT_FOUND
            );
        }
        $response = $this->json($this->masseEauExport($masseEau, $classementMasseEauRepository, $etatRepository, $etatMotifRepository, $classementStatutRepository));
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);
        return $response;
    }

    /**
     *
     * Retourne le contenu d'une masse d'eau en fonction de son code
     *
     * @Route("/masses-eau/code/{code}", name="masse-eau-code", methods={"GET"})
     *
     * @param string $code
     * @param MasseEauRepository $masseEauRepository
     * @param ClassementMasseEauRepository $classementMasseEauRepository
     * @param EtatRepository $etatRepository
     * @param EtatMotifRepository $etatMotifRepository
     * @param ClassementStatutRepository $classementStatutRepository
     * @return Response
     */
    public function showByMasseCode(
        string $code,
        MasseEauRepository $masseEauRepository,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        EtatMotifRepository $etatMotifRepository,
        ClassementStatutRepository $classementStatutRepository): Response
    {
        $masseEau = $masseEauRepository->findOneBy(['masseCode' => $code]);

        if (!$masseEau) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de masse d’eau ayant pour code ' . $masseEau
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        $response = $this->json($this->masseEauExport($masseEau, $classementMasseEauRepository, $etatRepository, $etatMotifRepository, $classementStatutRepository));
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);
        return $response;
    }

    /**
     *
     * Retourne le contenu d'une masse d'eau en fonction de l'id du bassin
     *
     * @Route("/masses-eau/bassin/{bassinId}", name="masse-eau-by-bassin-id", methods={"GET"})
     *
     * @param int $bassinId
     * @param MasseEauRepository $masseEauRepository
     * @param ClassementMasseEauRepository $classementMasseEauRepository
     * @param EtatRepository $etatRepository
     * @param EtatMotifRepository $etatMotifRepository
     * @param ClassementStatutRepository $classementStatutRepository
     * @return Response
     */
    public function showByBassinId(
        int $bassinId,
        MasseEauRepository $masseEauRepository,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        EtatMotifRepository $etatMotifRepository,
        ClassementStatutRepository $classementStatutRepository): Response
    {
        $massesEau = $masseEauRepository->findBy(['bassinId' => $bassinId]);

        if (!$massesEau) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de masse d’eau ayant pour un bassin avec l’id ' . $bassinId
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        foreach ($massesEau as $masseEau) {
            $massesEauResult[] = $this->masseEauLightExport($masseEau, $classementMasseEauRepository, $etatRepository, $etatMotifRepository, $classementStatutRepository);
        }

        $response = $this->json($massesEauResult);
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);
        return $response;
    }

    /**
     * Retourne une synthèse du contenu de "masseEau"
     *
     * @param MasseEau $masseEau
     * @return array
     */
    private function masseEauLightExport(
        MasseEau $masseEau,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        EtatMotifRepository $etatMotifRepository,
        ClassementStatutRepository $classementStatutRepository): array
    {
        $masseId = $masseEau->getMasseId();
        $bassinHydrographique = $masseEau->getBassinHydrographique();
        $masseEauDescription = $masseEau->getMasseEauDescription();
        $regionMarine = $masseEau->getRegionMarine();
        $departement = $masseEau->getDepartement();
        $atteinteObjectifDescription = $masseEau->getAtteinteObjectifDescription();
        $elementQualite = $masseEau->getElementQualite();
        $normalizedElementQualite = $this->normalizeElementQualiteLight($elementQualite, $classementMasseEauRepository, $etatRepository, $masseId);

        return [
            'id' => $masseId,
            'bassinId' => $bassinHydrographique->getBassinId(),
            'nom' => $masseEau->getMasseNom(),
            'code' => $masseEau->getMasseCode(),
            'type' => $masseEau->getMasseType(),
            'atteinteObjectifDescription' => $atteinteObjectifDescription ? [
                'libelle' => $atteinteObjectifDescription->getAtteinteObjectifLibelle(),
                'code' => $atteinteObjectifDescription->getAtteinteObjectifCode()
            ] : null,
            'description' => [
                'descId' => $masseEauDescription->getMasseDescId(),
                'libelle' => $masseEauDescription->getMasseDescLibelle(),
                'code' => $masseEauDescription->getMasseDescCode(),
                'color' => $masseEauDescription->getMasseDescColor(),
                'type' => $masseEauDescription->getMasseDescType(),
            ],
            'commentaire' => $masseEau->getMasseCommentaire() ? $masseEau->getMasseCommentaire() : null,
            'suivi' => $masseEau->getMasseBSuivi(),
            'risque' => $masseEau->getMasseBrisque(),
            'regionMarine' => $regionMarine->getRegionLibelle(),
            'departementsNom' => $this->getDepartementsNom($departement),
            'etat' => $this->sortEtatByElementQualite($normalizedElementQualite),
            'elementsQualite' => $this->normalizeElementQualite($elementQualite, $classementMasseEauRepository, $etatRepository, $etatMotifRepository, $classementStatutRepository, $masseId),
            'elementsQualiteEtat' => $this->normalizeElementQualiteEtat($elementQualite, $classementMasseEauRepository, $etatRepository, $masseId)
        ];
    }

    /**
     * retourne les états des masses d'eau par element qualité
     *
     * @param array $normalizedElementsQualites
     * @return array
     */
    private function sortEtatByElementQualite(array $normalizedElementsQualites): array
    {
        $etatsEcologiques = [];
        $etatsChimiques = [];

        foreach ($normalizedElementsQualites as $normalizedElementQualite) {

            if($normalizedElementQualite['etat'] && $normalizedElementQualite['etat']['valeur'] !== 8) {
                if ($normalizedElementQualite['typeClassementLibelle'] === 'écologique') {
                    $etatsEcologiques[] = $normalizedElementQualite['etat'];
                }
    
                if ($normalizedElementQualite['typeClassementLibelle'] === 'chimique') {
                    $etatsChimiques[] = $normalizedElementQualite['etat'];
                }
            }

        }

        $ecologique = $this->sortEtat($etatsEcologiques);
        $chimique = $this->sortEtat($etatsChimiques);

        $union = [
            $ecologique,
            $chimique
        ];
        
        $global = orderBy($union, ['valeur'], ['desc']);

        return [
            'ecologique' => $ecologique,
            'chimique' => $chimique,
            'global' => $global[0]
        ];
    }
    

    /**
     * Tri les états en fonction de la valeur
     *
     * @param array $etat
     * @return array|null
     */
    private function sortEtat(array $etat): ?array
    {
        $countedValeur = countBy($etat, function($etat) {return $etat['libelle'];});
        arsort($countedValeur);
        $firstKey = array_key_first($countedValeur);

        $inconnu = [];
        if ($firstKey === 'libelle') {
            foreach ($etat as $et) {
                if ($et['valeur'] === 0) {
                    $inconnu[] = $et;
                }
            }
        }

        $sortValeur = orderBy($etat, ['valeur'], ['desc']);
        return $sortValeur ? ($firstKey === 0 && count($countedValeur) > 3 ? $inconnu[0] : $sortValeur[0]) : null;
    }

    /**
     * Retourne le contenu de "masseEau"
     *
     * @param MasseEau $masseEau
     * @param ClassementMasseEauRepository $classementMasseEauRepository
     * @param EtatRepository $etatRepository
     * @return array
     */
    private function masseEauExport(
        MasseEau $masseEau,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        EtatMotifRepository $etatMotifRepository,
        ClassementStatutRepository $classementStatutRepository): array
    {
        $masseId = $masseEau->getMasseId();
        $masseEauDescription = $masseEau->getMasseEauDescription();
        $bassinHydrographique = $masseEau->getBassinHydrographique();
        $regionMarine = $masseEau->getRegionMarine();
        $departement = $masseEau->getDepartement();
        $atteinteObjectifDescription = $masseEau->getAtteinteObjectifDescription();
        $reseaux = $bassinHydrographique->getReseaux();
        $elementQualite = $masseEau->getElementQualite();
        $normalizedElementQualite = $this->normalizeElementQualite($elementQualite, $classementMasseEauRepository, $etatRepository, $etatMotifRepository, $classementStatutRepository, $masseId);
        $points = $masseEau->getPoints();
        $controles = $this->getPointsInfos($points);
        $masseDocDesc = $masseEau->getMasseDocDesc();

        return [
            'id' => $masseEau->getMasseId(),
            'code' => $masseEau->getMasseCode(),
            'nom' => $masseEau->getMasseNom(),
            'type' => $masseEau->getMasseType(),
            'commentaire' => $masseEau->getMasseCommentaire() ? $masseEau->getMasseCommentaire() : null,
            'suivi' => $masseEau->getMasseBSuivi(),
            'risque' => $masseEau->getMasseBrisque(),
            'fortModif' => $masseEau->getMasseBFortModif(),
            'description' => [
                'libelle' => $masseEauDescription->getMasseDescLibelle(),
                'code' => $masseEauDescription->getMasseDescCode(),
                'color' => $masseEauDescription->getMasseDescColor(),
                'type' => $masseEauDescription->getMasseDescType(),
            ],
            'bassinHydrographique' => [
                'id' => $bassinHydrographique->getBassinId(),
                'code' => $bassinHydrographique->getBassinCode(),
                'nom' => $bassinHydrographique->getBassinNom(),
                'alert' => $bassinHydrographique->getBassinAlert(),
            ],
            'regionMarine' => $regionMarine->getRegionLibelle(),
            'departementsNom' => $this->getDepartementsNom($departement),
            'atteinteObjectif' => $atteinteObjectifDescription ? [
                'libelle' => $atteinteObjectifDescription->getAtteinteObjectifLibelle(),
                'code' => $atteinteObjectifDescription->getAtteinteObjectifCode(),
                'color' => $atteinteObjectifDescription->getAtteinteObjectifColor()
            ] : null,
            'reseaux' => $this->normalizeReseau($reseaux),
            'etat' => $this->sortEtatByElementQualite($normalizedElementQualite),
            'elementsQualite' => $this->normalizeElementQualite($elementQualite, $classementMasseEauRepository, $etatRepository, $etatMotifRepository, $classementStatutRepository, $masseId),
            'elementsQualiteEtat' => $this->normalizeElementQualiteEtat($elementQualite, $classementMasseEauRepository, $etatRepository, $masseId),
            'controles' => $controles,
            'masseDocDesc' => $masseDocDesc
        ];
    }

    /**
     * Retourne la liste des reseaux groupes
     *
     * @param PersistentCollection $points
     * @return array
     */
    private function getPointsInfos(PersistentCollection $points): array
    {
        $reseauGroupeInfos = [];

        foreach ($points as $point) {
            $reseaux = $point->getReseaux();

            foreach ($reseaux as $reseau) {
                if ($reseau->getReseauGroupe()->getGroupeId() !== -1) {
                    $reseauCode = $reseau->getReseauCode();
                    $reseauNom = $reseau->getReseauNom();
                    $groupe = $reseau->getReseauGroupe();
                    $reseauGroupeInfos[] = [
                        'code' => $reseauCode,
                        'nom' => $reseauNom,
                        'groupe' => $groupe->getGroupeNom()
                    ];
                }
            }
        }

        return $reseauGroupeInfos;
    }

    /**
     * Retourne le(s) nom(s) du(des) département(s)
     *
     * @param PersistentCollection $departements
     * @return string
     */
    private function getDepartementsNom(PersistentCollection $departements): string
    {
        foreach ($departements as $departement) {
            $nomDepartements[] = $departement->getDeptNom();
        }

        return count($nomDepartements) > 1 ? join(', ', $nomDepartements) : $departement->getDeptNom();
    }

    /**
     * Retourne le contenu de "reseaux" sous forme de tableau associatif
     *
     * @param PersistentCollection $reseaux
     * @return array
     */
    private function normalizeReseau(PersistentCollection $reseaux): array
    {
        $reseauxResult = [];

        foreach ($reseaux as $reseau) {
            $reseauGroupe = $reseau->getReseauGroupe();

            $reseauxResult[] = [
                'code' => $reseau->getReseauCode(),
                'nom' => $reseau->getReseauNom(),
                'reseauGroupe' => $reseauGroupe && $reseauGroupe->getGroupeId() !== -1 ? $reseauGroupe->getGroupeNom() : null
            ];
        }

        return $reseauxResult;
    }

    /**
     * Retourne le contenu de "etat" sous forme de tableau associatif
     *
     * @param Etat $etat
     * @return array
     */
    private function normalizeEtat(Etat $etat): array
    {            
        return [
            'id' => $etat->getEtatId(),
            'libelle' => $etat->getEtatLibelle(),
            'couleur' => $etat->getEtatCouleur(),
            'valeur' => $etat->getEtatValeur()
        ];
    }

    /**
     * Retourne le contenu de "elementsQualite" sous forme de tableau associatif
     *
     * @param PersistentCollection $elementsQualite
     * @param ClassementMasseEauRepository $classementMasseEauRepository
     * @param EtatRepository $etatRepository
     * @param integer $masseId
     * @return array
     */
    private function normalizeElementQualite(
        PersistentCollection $elementsQualite,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        EtatMotifRepository $etatMotifRepository,
        ClassementStatutRepository $classementStatutRepository,
        int $masseId): array
    {
        $elementsQualiteResult = [];

        foreach ($elementsQualite as $elementQualite) {

            $elementQualiteId = $elementQualite->getElementQualiteId();
            $typeClassement = $elementQualite->getTypeClassement();
            $typeElementQualite = $elementQualite->getTypeElementQualite();
            $typeClassementElementLibelle = $typeClassement->getTypeClassementId() !== 0 ? $typeClassement->getTypeClassementLibelle() : null;
            $typeElementQualiteNom = $typeElementQualite->getTypeElementQualiteId() !== 0 ? $typeElementQualite->getTypeElementQualiteNom() : null;
            $docMethodo = $elementQualite->getElementQualiteDocMethodo();
            $docProtocole = $elementQualite->getElementQualiteDocProtocole();

            $classementMasseEau = $classementMasseEauRepository->findOneBy(['elementQualiteId' => $elementQualiteId, 'masseId' => $masseId]);
            $etatId = $classementMasseEau->getEtatId();
            $etat = $etatId ? $etatRepository->find($etatId) : null;
            $indiceConfiance = $classementMasseEau->getClassementIndiceConfiance();
            $motifId = $classementMasseEau->getMotifId();
            $motif = $motifId ? $etatMotifRepository->find($motifId) : null;
            $statutId = $classementMasseEau->getStatutId();
            $statut = $statutId ? $classementStatutRepository->find($statutId) : null;
            $classementBilan = $classementMasseEau->getClassementBilan();
            $classementComplementBilan = $classementMasseEau->getClassementComplementBilan();
            $classementDate = $classementMasseEau->getClassementDate();
            $classementDateMaj = $classementMasseEau->getClassementDateMaj();

            $elementsQualiteResult[] = [
                'id' => $elementQualiteId,
                'nom' => $elementQualite->getElementQualiteNom(),
                'niveau' => $elementQualite->getElementQualiteNiveau(),
                'parent' => $elementQualite->getElementQualiteParent(),
                'arbre' => $elementQualite->getElementQualiteArbre(),
                'rang' => $elementQualite->getElementQualiteRang(),
                'urlFicher' => $elementQualite->getElementQualiteUrlFiche(),
                'typeClassementLibelle' => $typeClassementElementLibelle,
                'typeElementQualiteNom' => $typeElementQualiteNom,
                'etat' => $etat ? $this->normalizeEtat($etat) : null,
                'indiceConfiance' => $indiceConfiance ? $indiceConfiance : null,
                'etatMotif' => $motif ? $this->normalizeEtatMotif($motif) : null,
                'statut' => $statut ? $this->normalizeClassementStatut($statut) : null,
                'docMethodo' => $docMethodo ? $docMethodo : null,
                'docProtocole' => $docProtocole ? $docProtocole : null,
                'classementBilan' => $classementBilan ? $classementBilan : null,
                'classementComplementBilan' => $classementComplementBilan ? $classementComplementBilan : null,
                'classementDate' => $classementDate ? $classementDate : null,
                'classementDateMaj' => $classementDateMaj ? $classementDateMaj : null
            ];
        }

        $unifiedElements = new ProcessElements($elementsQualiteResult);

        return $unifiedElements->unifiedElements();
    }

    /**
     * Retourne le contenu de "elementsQualite" sous forme de tableau associatif
     *
     * @param PersistentCollection $elementsQualite
     * @param ClassementMasseEauRepository $classementMasseEauRepository
     * @param EtatRepository $etatRepository
     * @param integer $masseId
     * @return array
     */
    private function normalizeElementQualiteLight(
        PersistentCollection $elementsQualite,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        int $masseId): array
    {
        $elementsQualiteResult = [];

        foreach ($elementsQualite as $elementQualite) {

            $elementQualiteId = $elementQualite->getElementQualiteId();
            $parent = $elementQualite->getElementQualiteParent();
            $typeClassement = $elementQualite->getTypeClassement();
            $typeClassementElementLibelle = $typeClassement->getTypeClassementId() !== 0 ? $typeClassement->getTypeClassementLibelle() : null;

            $classementMasseEau = $classementMasseEauRepository->findOneBy(['elementQualiteId' => $elementQualiteId, 'masseId' => $masseId]);
            $etatId = $classementMasseEau->getEtatId();
            $etat = $etatId ? $etatRepository->find($etatId) : null;

            $elementsQualiteResult[] = [
                'id' => $elementQualiteId,
                'parent' => $parent,
                'typeClassementLibelle' => $typeClassementElementLibelle,
                'etat' => $etat ? $this->normalizeEtat($etat) : null
            ];
        }

        $unifiedElements = new ProcessElements($elementsQualiteResult);

        return $unifiedElements->unifiedElements();
    }

    /**
     * Retourne l'id des elementsQualite avec leur etat
     *
     * @param PersistentCollection $elementsQualite
     * @param ClassementMasseEauRepository $classementMasseEauRepository
     * @param EtatRepository $etatRepository
     * @param integer $masseId
     * @return array
     */
    private function normalizeElementQualiteEtat(
        PersistentCollection $elementsQualite,
        ClassementMasseEauRepository $classementMasseEauRepository,
        EtatRepository $etatRepository,
        int $masseId): array
    {
        $elementsQualiteResult = [];

        foreach ($elementsQualite as $elementQualite) {

            $elementQualiteId = $elementQualite->getElementQualiteId();

            $classementMasseEau = $classementMasseEauRepository->findOneBy(['elementQualiteId' => $elementQualiteId, 'masseId' => $masseId]);
            $etatId = $classementMasseEau->getEtatId();
            $etat = $etatId ? $etatRepository->find($etatId) : null;

            $elementsQualiteResult[] = [
                'id' => $elementQualiteId,
                'etat' => $etat ? $this->normalizeEtat($etat) : null
            ];
        }

        return $elementsQualiteResult;
    }

    private function normalizeEtatMotif(EtatMotif $etatMotif)
    {
        return [
            'id' => $etatMotif->getMotifId(),
            'libelle' => $etatMotif->getMotifLibelle(),
            'code' => $etatMotif->getMotifCode()
        ];
    }

    private function normalizeClassementStatut(ClassementStatut $classementStatut)
    {
        return [
            'id' => $classementStatut->getStatutId(),
            'libelle' => $classementStatut->getStatutLibelle(),
            'code' => $classementStatut->getStatutCode()
        ];
    }

}
