<?php

namespace App\Controller;

use App\Entity\BassinHydrographique;
use App\Functions\ProcessElements;
use App\Repository\BassinHydrographiqueRepository;
use App\Repository\ElementQualiteSessionRepository;
use App\Repository\ReseauSessionRepository;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function _\groupBy;

class BassinHydrographiqueController extends AbstractController
{
    /**
     * Retourne le contenu de tous les bassins hydrographiques
     * 
     * @Route("/bassins", name="bassins", methods={"GET"})
     *
     * @param BassinHydrographiqueRepository $bassinHydrographiqueRepository
     * @return Response
     */
    public function showAll(BassinHydrographiqueRepository $bassinHydrographiqueRepository): Response
    {
        $bassins = $bassinHydrographiqueRepository->findAll();

        if (!$bassins) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Non trouvé'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        foreach ($bassins as $bassin) {
            $bassinsResult[] = $this->bassinExport($bassin);
        }

        $response = $this->json($bassinsResult);
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);

        return $response;
    }

    /**
     * Retourne le contenu d'un bassin en fonciton de son id
     * 
     * @Route("/bassins/{id}", name="bassin", methods={"GET"})
     *
     * @param integer $id
     * @param BassinHydrographiqueRepository $bassinHydrographiqueRepository
     * @return Response
     */
    public function show(int $id, BassinHydrographiqueRepository $bassinHydrographiqueRepository): Response
    {
        $bassin = $bassinHydrographiqueRepository->find($id);

        if (!$bassin) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de de bassin hydrographique ayant pour id ' . $id
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        $response = $this->json($this->bassinExport($bassin));
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);
        return $response;
    }

    /**
     * Retourne le contenu d'un bassin en fonction de son code
     *
     * @Route("/bassins/code/{code}", name="bassin_by_code", methods={"GET"})
     *
     * @param string $code
     * @param BassinHydrographiqueRepository $bassinHydrographiqueRepository
     * @return Response
     */
    public function showByCode(string $code, BassinHydrographiqueRepository $bassinHydrographiqueRepository): Response
    {
        $bassin = $bassinHydrographiqueRepository->findOneBy(['bassinCode' => $code]);

        if (!$bassin) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de de bassin hydrographique ayant pour code ' . $code
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        $response = $this->json($this->bassinExport($bassin));
        $response->setSharedMaxAge($_SERVER['CACHE_HOUR_MAX_AGE'] * 3600);
        return $response;
    }

    /**
     * Retourne le contenu d'un bassin en fonciton de son id et de la session
     * 
     * @Route("/bassins/{id}/session/{sessionId}", name="bassin_session", methods={"GET"})
     *
     * @param integer $id
     * @param integer $sessionId
     * @param BassinHydrographiqueRepository $bassinHydrographiqueRepository
     * @param ElementQualiteSessionRepository $elementQualiteSessionRepository
     * @param ReseauSessionRepository $reseauSessionRepository
     * @return Response
     */
    public function showBySession(
        int $id,
        int $sessionId,
        BassinHydrographiqueRepository $bassinHydrographiqueRepository,
        ElementQualiteSessionRepository $elementQualiteSessionRepository,
        ReseauSessionRepository $reseauSessionRepository): Response
    {
        $bassin = $bassinHydrographiqueRepository->find($id);

        if (!$bassin) {
            return $this->json(
                [
                    'code' => 404,
                    'message' => 'Pas de de bassin hydrographique ayant pour id ' . $id
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        $elementQualiteSession = $elementQualiteSessionRepository->findBy(['sessionId' => $sessionId, 'bassinId' => $id]);
        $reseauSession = $reseauSessionRepository->findBy(['sessionId' => $sessionId, 'bassinId' => $id]);

        return $this->json($this->bassinExport($bassin, $elementQualiteSession, $reseauSession));
    }

    /**
     * Retourne le contenu d'un bassin
     *
     * @param BassinHydrographique $bassin
     * @param array $elementQualiteSession
     * @param array $reseauSession
     * @return array
     */
    private function bassinExport(BassinHydrographique $bassin, array $elementQualiteSession = null, array $reseauSession = null): array
    {

        if ($elementQualiteSession === null) {
            $elementsQualite = $this->normalizeElementQualite($bassin->getElementQualite());
        } else if (count($elementQualiteSession) > 0) {
            $elementsQualite = $this->normalizeElementQualiteSession($elementQualiteSession);
        } else {
            $elementsQualite = [];
        }

        if ($reseauSession === null) {
            $reseaux = $this->normalizeReseaux($bassin->getReseaux());
        } else if (count($reseauSession) > 0) {
            $reseaux = $this->normalizeReseauxSession($reseauSession);
        } else {
            $reseaux = [];
        }

        $massesEau = $bassin->getMassesEau();

        return [
            'id' => $bassin->getBassinId(),
            'code' => $bassin->getBassinCode(),
            'nom' => $bassin->getBassinNom(),
            'XMin' => $bassin->getBassinXmin(),
            'XMax' =>  $bassin->getBassinXmax(),
            'YMin' => $bassin->getBassinYmin(),
            'YMax' =>  $bassin->getBassinYmax(),
            'alert' => $bassin->getBassinAlert(),
            'alertCarte' => $bassin->getBassinAlertCarte(),
            'urlWms' => $bassin->getBassinUrlWms(),
            'layerWms' => $bassin->getBassinLayerWms(),
            'bassinProjection' => $bassin->getBassinProjection(),
            'dateMaj' => $bassin->getDateMaj(),
            'massesEau' => $this->normalizeMassesEau($massesEau),
            'elementsQualite' => $elementsQualite,
            'reseaux' => $reseaux
        ];
    }

    /**
     * Retourne le contenu de "elementsQualite" sous forme de tableau associatif
     *
     * @param PersistentCollection $elementsQualite
     * @return array
     */
    private function normalizeElementQualite(PersistentCollection $elementsQualite): array
    {
        $elementsQualiteResult = [];

        foreach ($elementsQualite as $elementQualite) {

            $typeClassement = $elementQualite->getTypeClassement();
            $typeElementQualite = $elementQualite->getTypeElementQualite();
            $typeClassementElementLibelle = $typeClassement->getTypeClassementId() !== 0 ? $typeClassement->getTypeClassementLibelle() : null;
            $typeElementQualiteNom = $typeElementQualite->getTypeElementQualiteId() !== 0 ? $typeElementQualite->getTypeElementQualiteNom() : null;

            $elementsQualiteResult[] = [
                'id' => $elementQualite->getElementQualiteId(),
                'nom' => $elementQualite->getElementQualiteNom(),
                'niveau' => $elementQualite->getElementQualiteNiveau(),
                'parent' => $elementQualite->getElementQualiteParent(),
                'arbre' => $elementQualite->getElementQualiteArbre(),
                'rang' => $elementQualite->getElementQualiteRang(),
                'urlFicher' => $elementQualite->getElementQualiteUrlFiche(),
                'typeClassementLibelle' => $typeClassementElementLibelle,
                'typeElementQualiteNom' => $typeElementQualiteNom
            ];
        }

        $unifiedElements = new ProcessElements($elementsQualiteResult);

        return $unifiedElements->unifiedElements();
    }

    /**
     * Retourne le contenu de "elementsQualite" sous forme de tableau associatif
     *
     * @param array $elementsQualite
     * @return array
     */
    private function normalizeElementQualiteSession(array $elementsQualiteSession): array
    {
        $elementsQualiteResult = [];
        $typeClassementsResult = [];
        $typeElementQualitesResult = [];

        foreach ($elementsQualiteSession as $elementQualite) {

            $typeClassements = $elementQualite->getTypeClassement();
            $typeElementQualites = $elementQualite->getTypeElementQualite();
            
            foreach ($typeClassements as $typeClassement) {
                $typeClassementsResult = $typeClassement->getTypeClassementLibelle();
            }

            foreach ($typeElementQualites as $typeElementQualite) {
                $typeElementQualitesResult =  $typeElementQualite->getTypeElementQualiteNom();
            }

            $elementsQualiteResult[] = [
                'id' => $elementQualite->getElementQualiteId(),
                'nom' => $elementQualite->getElementQualiteNom(),
                'niveau' => $elementQualite->getElementQualiteNiveau(),
                'parent' => $elementQualite->getElementQualiteParent(),
                'arbre' => $elementQualite->getElementQualiteArbre(),
                'rang' => $elementQualite->getElementQualiteRang(),
                'urlFicher' => $elementQualite->getElementQualiteUrlFiche(),
                'typeClassementLibelle' => $typeClassementsResult,
                'typeElementQualiteNom' => $typeElementQualitesResult
            ];
        }
        return $elementsQualiteResult;
    }

    /**
     * Retourne le contenu de "etats" sous forme de tableau associatif
     *
     * @param PersistentCollection $etats
     * @return array
     */
    private function normalizeEtat(PersistentCollection $etats): array
    {
        $etatsResult = [];

        foreach ($etats as $etat) {

            $etatsResult[] = [
                'id' => $etat->getEtatId(),
                'libelle' => $etat->getEtatLibelle(),
                'couleur' => $etat->getEtatCouleur(),
                'valeur' => $etat->getEtatValeur(),
            ];
        }

        return $etatsResult;
    }

    /**
     * Retourne le contenu de "reseaux" sous forme de tableau associatif
     *
     * @param PersistentCollection $reseaux
     * @return array
     */
    private function normalizeReseaux(PersistentCollection $reseaux): array
    {
        $reseauxResult = [];

        foreach ($reseaux as $reseau) {

            $points = $reseau->getPoints();
            $reseauGroupe = $reseau->getReseauGroupe();
            $pointsResult = [];

            foreach ($points as $point) {
                $pointsResult[] = [
                    'id' => $point->getPointId(),
                    'code' => $point->getPointCode(),
                    'nom' => $point->getPointNom()
                ];
            }

            if(count($pointsResult) > 0) {
                $reseauxResult[] = [
                    'id' => $reseau->getReseauId(),
                    'nom' => $reseau->getReseauNom(),
                    'symbole' => $reseau->getReseauSymbole() !== '-1' ? $reseau->getReseauSymbole() : 'octagon',
                    'symboleTaille' => $reseau->getReseauSymboleTaille(),
                    'couleur' => $reseau->getReseauCouleur(),
                    'imgSymb' => $reseau->getReseauSymbole() === '-1' ? $reseau->getReseauImgSymb() : null,
                    'reseauGroupeNom' => $reseauGroupe->getGroupeId() !== -1 ? $reseauGroupe->getGroupeNom() : 'reseauGroupe',
                    'points' => $pointsResult
                ];
            }
        }

        return groupBy($reseauxResult, 'reseauGroupeNom');
    }

    /**
     * Retourne le contenu de "reseauxSession" sous forme de tableau associatif
     *
     * @param array $reseauxSession
     * @return array
     */
    private function normalizeReseauxSession(array $reseauxSession): array
    {
        $reseauxResult = [];

        foreach ($reseauxSession as $reseau) {

            $points = $reseau->getPoints();
            $pointsResult = [];

            foreach ($points as $point) {
                $pointsResult[] = [
                    'id' => $point->getPointId(),
                    'code' => $point->getPointCode(),
                    'nom' => $point->getPointNom()
                ];
            }

            $reseauxResult[] = [
                'id' => $reseau->getReseauId(),
                'nom' => $reseau->getReseauNom(),
                'symbole' => $reseau->getReseauSymbole() !== '-1' ? $reseau->getReseauSymbole() : null,
                'symboleTaille' => $reseau->getReseauSymboleTaille(),
                'couleur' => $reseau->getReseauCouleur(),
                'imgSymb' => $reseau->getReseauSymbole() === '-1' ? $reseau->getReseauImgSymb() : null,
                'points' => $pointsResult
            ];
        }

        return $reseauxResult;
    }

    /**
     * Retourne le contenu de "massesEau" sous forme de tableau associatif
     *
     * @param PersistentCollection $massesEau
     * @return array
     */
    private function normalizeMassesEau(PersistentCollection $massesEau): array
    {
        $massesEauResult = [];

        foreach ($massesEau as $masseEau) {

            $massesEauResult[] = [
                'id' => $masseEau->getMasseId(),
                'code' => $masseEau->getMasseCode(),
                'nom' => $masseEau->getMasseNom(),
                'type' => $masseEau->getMasseType(),
                'masseTypeSuivi' => $masseEau->getMasseTypeSuivi(),
                'masseBSuivi' => $masseEau->getMasseBSuivi()
            ];
        }

        return $massesEauResult;
    }

}
