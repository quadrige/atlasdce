<?php

namespace App\Repository;

use App\Entity\ReseauSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReseauSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReseauSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReseauSession[]    findAll()
 * @method ReseauSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReseauSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReseauSession::class);
    }

    // /**
    //  * @return ReseauSession[] Returns an array of ReseauSession objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReseauSession
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
