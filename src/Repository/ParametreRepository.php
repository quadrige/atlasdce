<?php

namespace App\Repository;

use App\Entity\Parametre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ParametreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Parametre::class);
    }

    /**
     * Compte le nombre de paramètre en fonction de leur code
     *
     * @return integer
     */
    public function countParametre(): int
    {
        $qbParametre = $this->createQueryBuilder('p')
            ->select('p.parametreCode')
            ->groupBy('p.parametreCode');

        $countParametre = $qbParametre->getQuery()->getScalarResult();
        return (int) count($countParametre);
    }

}
