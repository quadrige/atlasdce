<?php

namespace App\Repository;

use App\Entity\ElementQualiteSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ElementQualiteSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method ElementQualiteSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method ElementQualiteSession[]    findAll()
 * @method ElementQualiteSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElementQualiteSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ElementQualiteSession::class);
    }

    // /**
    //  * @return ElementQualiteSession[] Returns an array of ElementQualiteSession objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ElementQualiteSession
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
