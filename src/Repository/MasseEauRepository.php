<?php

namespace App\Repository;

use App\Entity\MasseEau;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MasseEauRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MasseEau::class);
    }

    /**
     * Compte le nombre de masse d'eau cotière et de transition
     *
     * @return array
     */
    public function countMasseType(): array
    {
        $qbMEC = $this->createQueryBuilder('m')
            ->select('COUNT(m)')
            ->where('m.masseType = :cotiere')
            ->setParameter('cotiere', 'MEC');

        $qbMET = $this->createQueryBuilder('m')
            ->select('COUNT(m)')
            ->where('m.masseType = :transition')
            ->setParameter('transition', 'MET');

        $countMEC = $qbMEC->getQuery()->getSingleScalarResult();
        $countMET = $qbMET->getQuery()->getSingleScalarResult();

        return [
            'cotiere' => (int) $countMEC,
            'transition' => (int) $countMET
        ];

    }

    public function countMassTypeByBassinId(int $bassinId): array
    {
        $qbMEC = $this->createQueryBuilder('m')
            ->select('COUNT(m)')
            ->where('m.masseType = :cotiere')
            ->andWhere('m.bassinId = :bassinId')
            ->setParameters(['cotiere' => 'MEC', 'bassinId' => $bassinId]);
        
        $qbMET = $this->createQueryBuilder('m')
            ->select('COUNT(m)')
            ->where('m.masseType = :transition')
            ->andWhere('m.bassinId = :bassinId')
            ->setParameters(['transition' => 'MET', 'bassinId' => $bassinId]);

        $countMEC = $qbMEC->getQuery()->getSingleScalarResult();
        $countMET = $qbMET->getQuery()->getSingleScalarResult();

        return [
            'cotiere' => (int) $countMEC,
            'transition' => (int) $countMET
        ];
    }

}
