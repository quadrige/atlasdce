<?php

namespace App\Repository;

use App\Entity\ClassementMasseEau;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClassementMasseEau|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClassementMasseEau|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClassementMasseEau[]    findAll()
 * @method ClassementMasseEau[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassementMasseEauRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClassementMasseEau::class);
    }

    // /**
    //  * @return ClassementMasseEau[] Returns an array of ClassementMasseEau objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClassementMasseEau
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
