<?php

namespace App\Repository;

use App\Entity\Point;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Point::class);
    }

    /**
     * Compte le nombre de point de surveillance
     *
     * @return integer
     */
    public function countPoint(): int
    {
        $qbPoint = $this->createQueryBuilder('c')
            ->select('COUNT(c.pointCode)');

        $countPoint = $qbPoint->getQuery()->getSingleScalarResult();
        return (int) $countPoint;
    }

    public function countPointByMassesEau(array $massesIds)
    {
        $qbPoint = $this->createQueryBuilder('c');
        $qbPoint->select('COUNT(c)')
            ->setParameter('massesIds', $massesIds)
            ->andWhere($qbPoint->expr()->in('c.masseId', ':massesIds'));
        
        $countPoint = $qbPoint->getQuery()->getSingleScalarResult();
        return $countPoint;
    }

}
