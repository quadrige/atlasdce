<?php

namespace App\Repository;

use App\Entity\TypeElementQualite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeElementQualite|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeElementQualite|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeElementQualite[]    findAll()
 * @method TypeElementQualite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeElementQualiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeElementQualite::class);
    }

}
