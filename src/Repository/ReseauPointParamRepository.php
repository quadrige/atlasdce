<?php

namespace App\Repository;

use App\Entity\ReseauPointParam;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ReseauPointParamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReseauPointParam::class);
    }

    public function findReseauxByPointId(int $pointId): array
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.pointId = :pointId')
            ->setParameter('pointId', $pointId);

        return $qb->getQuery()->getScalarResult();
    }

}
