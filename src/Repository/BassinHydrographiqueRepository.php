<?php

namespace App\Repository;

use App\Entity\BassinHydrographique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class BassinHydrographiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BassinHydrographique::class);
    }

    /**
     * Compte distinctement les bassins de métropole et des outremers
     *
     * @return array
     */
    public function countBassin(): array
    {
        $codeOutremers = BassinHydrographique::CODE_BASSINS_OUTREMERS;
        $qbBassinsOutremers = $this->createQueryBuilder('b');
        $qbBassinsMetropole = $this->createQueryBuilder('b');

        $qbBassinsOutremers->select('COUNT(b)')
            ->setParameter('codes', $codeOutremers)
            ->andWhere($qbBassinsOutremers->expr()->in('b.bassinCode', ':codes'));

        $qbBassinsMetropole->select('COUNT(b)')
            ->setParameter('codes', $codeOutremers)
            ->andWhere($qbBassinsMetropole->expr()->notin('b.bassinCode', ':codes'));
        
        $countBassinsOutremers = $qbBassinsOutremers->getQuery()->getSingleScalarResult();
        $countBassinsMetropole = $qbBassinsMetropole->getQuery()->getSingleScalarResult();

        return [
            'bassinsOutremers' => (int) $countBassinsOutremers,
            'bassinsMetropole' => (int) $countBassinsMetropole
        ];
    }

}
