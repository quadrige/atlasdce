## Intégration continue

Le processus d'intégration continue permet de générer l'image docker à partir des sources SVN du projet.

| Etape | Tâches | Description | Executeur |
|-|-|-|-|
| package | docker:develop | Création d'une image docker taggé **develop** permettant d'éxecuter l'application | push sur branche master |
| package | docker:tag | Création d'une image docker taggé **CI_COMMIT_TAG** permettant d'éxecuter l'application | création d'un tag |

L'image ainsi générée peut ensuite être déployé sur n'importe quel machine avec docker installé.

## Variables

- *`MYSQL_HOST` hôte de la base de données*
- *`MYSQL_PORT` port de la base de données*
- *`MYSQL_DATABASE` nom de la base de données*
- *`MYSQL_USER` utilisateur liée à la base de données*
- *`MYSQL_PASSWORD` mot de passe de l'utilisateur* 

- *`DATABASE_URL` correspond aux informations de la base de donnée*
  - exemple : DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"

Les variables pourront être complétées (ou écrasée) suite à la commande `docker run` ou `docker-compose` (ou `docker compose`) :

```bash
docker run -e VARIABLE=VALUE ...
```
ou

```bash
docker-compose --env-file .env.local up
```

## Démarrer l'API

### En buildant l'image en local

Localement avec docker composer et le fichier environnement pas defaut (`.env`)

```bash
docker-compose up 
```

Ou en utilisant le script d'execution :

```bash
docker compose build
./docker-run.sh
```

/!\ à bien déposer un dump de la base de données dans le répertoire `./database/` du projet pour que la base de données soit initialisée.

### En récupérant l'image sur la registry

Commande à exécuter depuis une machine unix avec docker installé :

```bash
./docker-run.sh .env.registry
```
___

# Mise en production et développement (debian ou ubuntu)

## Pré-requis
- Composer
- PHP >= 7.3.19
- PHP extensions :
  - [ctype](https://www.php.net/book.ctype)
  - [iconv](https://www.php.net/book.iconv)
  - [JSON](https://www.php.net/book.json)
  - [Session](https://www.php.net/book.pcre)
  - [SimpleXML](https://www.php.net/book.simplexml)
  - [Tokenizer](https://www.php.net/book.tokenizer)
- MySQL >= 5.7

## Variables d'environnement (fichier `.env.local` à créer et modifier)
```
cp .env .env.local
```
- *`MYSQL_HOST` hôte de la base de données*
- *`MYSQL_PORT` port de la base de données*
- *`MYSQL_DATABASE` nom de la base de données*
- *`MYSQL_USER` utilisateur liée à la base de données*
- *`MYSQL_PASSWORD` mot de passe de l'utilisateur* 
- *`DATABASE_URL` informations de la base de donnée*
  - exemple : DATABASE_URL="mysql://db_user:db_password@db_host:db_port/db_name?serverVersion=5.7"
- *`ALLOWED_DOMAIN` adresse du front atlasdce*
- *`CACHE_HOUR_MAX_AGE` nombre d'heure de la mise en cache*

- # Mise en production

### Changer le fichier `.env.local` en `.env.local.php` et définir l'environnement de prod 

```
composer dump-env prod
```

### Intallation des dépendances pour la production
```
composer install --no-dev --optimize-autoloader
```

- ## Apache

### Générer un fichier .htaccess (facultatif)
```
composer require symfony/apache-pack
```

Les différentes configurations possible sont dans le dossier [README/apache-config](./README/apache-config)

Plus d'exemple de configuration possible sur [symfony.com](https://symfony.com/doc/current/setup/web_server_configuration.html#apache-with-php-fpm)

- ## Nginx

Pour nginx un exemple de configuration est disponible dans le dossier [README/nginx](./README/nginx)

## ⚠️ Erreur possible sur debian

Sur un serveur debian, il est possible que l'application retourne une erreur en essayant de créer les fichiers mis en caches : 
- `PHP Fatal error:  Uncaught RuntimeException: Unable to create the store directory (app_directory/atlasdce/var/cache/prod/http_cache)`

Si c'est le cas, se reporter à la documentation officielle : [Setting up or Fixing File Permissions](https://symfony.com/doc/current/setup/file_permissions.html)


- # Développement

## Installation des dépendances
```
composer i
```

### Lancer les tests
```
php bin/phpunit
```

### Lancement du server

```
php -S 127.0.0.1:8000 -t public
```

# Points d'accès

### `/stats`
- `GET` : Retourner quelques chiffres clés

### `/masses-eau`
- `GET` : Lister les masses-d'eau

### `/masses-eau/{id}`
- `GET` : Récupérer les information d'une masse d'eau

### `/bassins`
- `GET` : Lister les bassins hydrographiques

### `/bassins/{id}`
- `GET` : Récupérer les information d'un bassin hydrographique

### `/points`
- `GET` : Récupérer tous les points

### `/points/{id}`
- `GET` : Récupérer un point

### `/reseaux`
- `GET` : Lister les réseaux

### `/reseaux/{id}`
- `GET` : Récupérer les informations d'un réseau

### `/parametres`
- `GET` : Lister les paramètres

### `/parametres/{id}`
- `GET` : Récupérer les informations d'un parametre

### `/reseau-point-param/{pointId}`
- `GET` : Récupérer les informations des paramètres d'un point
