#!/bin/bash

SCRIPT_DIR=$(dirname $0)
BASEDIR=$(cd "${SCRIPT_DIR}" && pwd -P)

ENV_FILE=$1
[[ "_${ENV_FILE}" = "_" ]] && ENV_FILE=.env

CI_REGISTRY=gitlab-registry.ifremer.fr
CI_REGISTER_USER=gitlab+deploy-token-ro
CI_REGISTER_PWD=m-VKgWpGizGoiyZBmSK9

# check if env file exists
if [[ -z "${ENV_FILE}" ]]
then
    echo "[WARN] La variable ENV_FILE n'est pas définie, le fichier d'environnement par défaut est utilisé" >&2
else
    if [[ ! -f "${ENV_FILE}" ]]; then
        echo "[ERROR] Le fichier ${ENV_FILE} n'existe pas !" >&2
        exit 5
    fi
    echo "[INFO] Utilisation du fichier d'environnement suivant : ${ENV_FILE}" >&2
fi

echo "[INFO] Login to ${CI_REGISTRY}..."
echo ${CI_REGISTER_PWD} | docker login ${CI_REGISTRY} --password-stdin -u ${CI_REGISTER_USER}
[[ $? -ne 0 ]] && exit 1

echo "[INFO] Stop and remove previous containers..."
docker compose --env-file ${ENV_FILE} down

echo "[INFO] Execute docker compose with ${ENV_FILE}..."
docker compose --env-file ${ENV_FILE} up -d --no-build

echo "[INFO] logout from ${CI_REGISTRY}..."
docker logout ${CI_REGISTRY}

echo "---- ${CI_PROJECT_NAME} is running !"
echo ""
echo " Available commands:"
echo "  status: docker compose ps"
echo "    logs: docker compose logs -f"
echo "    exec: docker exec container_name bash"
echo "    stop: docker compose down"
