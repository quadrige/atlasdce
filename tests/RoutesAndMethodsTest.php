<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RoutesAndMethodsTests extends WebTestCase
{
    /**
     * @dataProvider routesProviderGETMethod
     */
    public function testResponseIsSucessful($method, $url)
    {
        $client = self::createClient();
        $client->request($method, $url);

        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider routesProviderGETMethod
     */
    public function testResponseContentType($method, $url)
    {
        $client = static::createClient();
        $client->request($method, $url);

        $this->assertResponseHeaderSame('Content-Type', 'application/json');
    }

    /**
     * @dataProvider routesProviderOtherMethods
     */
    public function testNotAllowedMethod($method, $url)
    {
        $client = self::createClient();
        $client->request($method, $url);

        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * @dataProvider routesProviderInvalidTypeArguments
     */
    public function testException($method, $url)
    {
        $this->expectException(\TypeError ::class);
        $client = self::createClient();
        $client->request($method, $url);
    }

    public function routesProviderGETMethod()
    {
        return [
            'GET /stats' => ['GET', '/stats'],
            'GET /masses-eau' => ['GET', '/masses-eau'],
            'GET /masses-eau/1' => ['GET', '/masses-eau/1'],
            'GET /bassins' => ['GET', '/bassins'],
            'GET /bassins/1' => ['GET', '/bassins/1'],
            'GET /bassins/1/session/1' => ['GET', '/bassins/1/session/1'],
            'GET /points' => ['GET', '/points'],
            'GET /points/1' => ['GET', '/points/1'],
            'GET /reseaux' => ['GET', '/reseaux'],
            'GET /reseaux/1' => ['GET', '/reseaux/1'],
            'GET /parametres' => ['GET', '/parametres'],
            'GET /parametres/1' => ['GET', '/parametres/1'],
        ];
    }

    public function routesProviderOtherMethods()
    {
        return [
            'POST /stats' => ['POST', '/stats'],
            'POST /masses-eau' => ['POST', '/masses-eau'],
            'POST /masses-eau/1' => ['POST', '/masses-eau/1'],
            'POST /bassins' => ['POST', '/bassins'],
            'POST /bassins/1' => ['POST', '/bassins/1'],
            'POST /bassins/1/session/1' => ['POST', '/bassins/1/session/1'],
            'POST /points' => ['POST', '/points'],
            'POST /points/1' => ['POST', '/points/1'],
            'POST /reseaux' => ['POST', '/reseaux'],
            'POST /reseaux/1' => ['POST', '/reseaux/1'],
            'POST /parametres' => ['POST', '/parametres'],
            'POST /parametres/1' => ['POST', '/parametres/1'],
            'PUT /stats' => ['PUT', '/stats'],
            'PUT /masses-eau' => ['PUT', '/masses-eau'],
            'PUT /masses-eau/1' => ['PUT', '/masses-eau/1'],
            'PUT /bassins' => ['PUT', '/bassins'],
            'PUT /bassins/1' => ['PUT', '/bassins/1'],
            'PUT /bassins/1/session/1' => ['PUT', '/bassins/1/session/1'],
            'PUT /points' => ['PUT', '/points'],
            'PUT /points/1' => ['PUT', '/points/1'],
            'PUT /reseaux' => ['PUT', '/reseaux'],
            'PUT /reseaux/1' => ['PUT', '/reseaux/1'],
            'PUT /parametres' => ['PUT', '/parametres'],
            'PUT /parametres/1' => ['PUT', '/parametres/1'],
            'PATCH /stats' => ['PATCH', '/stats'],
            'PATCH /masses-eau' => ['PATCH', '/masses-eau'],
            'PATCH /masses-eau/1' => ['PATCH', '/masses-eau/1'],
            'PATCH /bassins' => ['PATCH', '/bassins'],
            'PATCH /bassins/1' => ['PATCH', '/bassins/1'],
            'PATCH /bassins/1/session/1' => ['PATCH', '/bassins/1/session/1'],
            'PATCH /points' => ['PATCH', '/points'],
            'PATCH /points/1' => ['PATCH', '/points/1'],
            'PATCH /reseaux' => ['PATCH', '/reseaux'],
            'PATCH /reseaux/1' => ['PATCH', '/reseaux/1'],
            'PATCH /parametres' => ['PATCH', '/parametres'],
            'PATCH /parametres/1' => ['PATCH', '/parametres/1'],
            'DELETE /stats' => ['DELETE', '/stats'],
            'DELETE /masses-eau' => ['DELETE', '/masses-eau'],
            'DELETE /masses-eau/1' => ['DELETE', '/masses-eau/1'],
            'DELETE /bassins' => ['DELETE', '/bassins'],
            'DELETE /bassins/1' => ['DELETE', '/bassins/1'],
            'DELETE /bassins/1/session/1' => ['DELETE', '/bassins/1/session/1'],
            'DELETE /points' => ['DELETE', '/points'],
            'DELETE /points/1' => ['DELETE', '/points/1'],
            'DELETE /reseaux' => ['DELETE', '/reseaux'],
            'DELETE /reseaux/1' => ['DELETE', '/reseaux/1'],
            'DELETE /parametres' => ['DELETE', '/parametres'],
            'DELETE /parametres/1' => ['DELETE', '/parametres/1']
        ];
    }

    public function routesProviderInvalidTypeArguments()
    {
        return [
            'GET /masses-eau/a' => ['GET', '/masses-eau/a'],
            'GET /bassins/b' => ['GET', '/bassins/b'],
        ];
    }

}
