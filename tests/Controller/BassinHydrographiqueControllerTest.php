<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class BassinHydrographiqueControllerTest extends WebTestCase
{
    /**
     * @dataProvider routesProviderBassinNotFound
     */
    public function testNotFound($method, $url)
    {
        $client = self::createClient();
        $client->request($method, $url);

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function routesProviderBassinNotFound()
    {
        return [
            'GET /bassins/4332' => ['GET', '/bassins/4332'],
        ];
    }
}
