<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class MasseEauControllerTest extends WebTestCase
{
    /**
     * @dataProvider routesProviderMasseEauNotFound
     */
    public function testNotFound($method, $url)
    {
        $client = self::createClient();
        $client->request($method, $url);

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function routesProviderMasseEauNotFound()
    {
        return [
            'GET /masses-eau/4332' => ['GET', '/masses-eau/4332'],
        ];
    }
}
